
/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: ClienteSubasta.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/
public class ClienteSubasta
{
    
    /**
     * Descripción: Método encargado de iniciar el funcionamiento del programa. 
     * 
     */
    public static void main(String[] args) 
    {
        InterfazCliente interfazCliente= new InterfazCliente();
        InterfazIngresoUsuario interfazIngreso = new InterfazIngresoUsuario();
        Controlador control = new Controlador(interfazCliente, interfazIngreso);
        HiloGrupal hilo = new HiloGrupal(interfazCliente, interfazIngreso);
        hilo.start();

    }
    
}
