
/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: HiloGrupal.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Level;
import java.util.logging.Logger;



//Instanciamiento de las variables
public class HiloGrupal extends Thread
{
    
    InterfazCliente interfazCliente;
    InterfazIngresoUsuario interfazIngresoUsuario;
    
    
    //Costructor de la clase.
    public HiloGrupal(InterfazCliente vistaCliente, InterfazIngresoUsuario vistaIngresoUsuario)
    {
        this.interfazCliente = vistaCliente;
        this.interfazIngresoUsuario = vistaIngresoUsuario;
    
    }   
    
    
    
    
    /**
     * Descripción: Método que se usa heredando de la clase thread, para ejecutar un subproceso, en 
     *              este caso el método actualizarReloj(), con esto se logra actualizar el reloj cada
     *              segundo.
     * Parámetros de entrada: Ninguno.
     * Parámetros de salida: Ninguno.
     */
    @Override
    public void run()
    {
        actualizarReloj();
    }

    
    /**
     * Descripción: Metodo encargado realizar una conexión con el servidor por medio de una dirección
     *              de grupo y recibir un arreglo de bytes con la hora que el servidor le envíe,
     *              una vez que la recibe, la ingresa en la interfaz de ingreso de usuario y en la
     *              interfaz del cliente.
     * Parámetros de entrada: Ninguno.
     * Parámetros de salida: Ninguno.
     */
    public void actualizarReloj()
    {
        try 
        {
            MulticastSocket conexion = new MulticastSocket(4000);
            conexion.joinGroup(InetAddress.getByName("235.1.1.1"));
            byte [] reloj = new byte [100];
            DatagramPacket data = new DatagramPacket(reloj, reloj.length);
            
            while (true) 
            {
                conexion.receive(data);
                String hora = new String(data.getData());
                interfazIngresoUsuario.lHora.setText(hora);
                interfazCliente.lHora.setText(hora);
                
            }
            
        } 
        catch (Exception e)
        {
            System.out.println("Error al actualizar reloj");
        }
    }        
            
}
