/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: Subasta.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.sql.Time;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.Timer;

//Instaciamiento de variables.
public class HiloInformado extends Thread
{
    
    InterfazCliente interfazCliente;
    Modelo modelo;
    
    
    //Costructor de la clase.
    public HiloInformado(InterfazCliente vistaCliente,Modelo modelo)
    {
        this.interfazCliente = vistaCliente;
        this.modelo = modelo;
    }   
    
    
    
    
     /**
     * Descripción: Método que se usa heredando de la clase thread, para ejecutar un subproceso, en 
     *              este caso el método actualizarSubastas, con esto se logra actualizar la subasta cada
     *              segundo.
     * Parámetros de entrada: Ninguno.
     * Parámetros de salida: Ninguno.
     */


    public void run()
    {
        System.out.println("hiloInformado.run()");
        actualizarSubastas();
    }

    
     /**
     * Descripción: Metodo encargado realizar una conexión con el servidor por medio de una dirección
     *              de grupo y recibir un arreglo de bytes con la petició de que actualize los items de una subasta
     *              en proceso.
     *              Una vez que la recibe, se empienzan a llamar los métodos que realicen la acción que solicitó la 
     *              petición recibida.
     *              Esta puede ser que se actualice el nuémro de participantes de una subasta, la cantidad de ofertas,
     *              o bien, que de por terminada la subasta.
     * Parámetros de entrada: Ninguno.
     * Parámetros de salida: Ninguno.
     */
    public void actualizarSubastas()
    {
        try 
        {
            MulticastSocket conexion = new MulticastSocket(4001);
            conexion.joinGroup(InetAddress.getByName("235.1.1.2"));
            byte [] b = new byte [100];
            DatagramPacket data = new DatagramPacket(b, b.length);
            
            while (true) 
            {
                conexion.receive(data);
                String mensaje = new String(data.getData());
                String[] mensajes = mensaje.split(":");
                String instruccion  = mensajes[0];
                int codigo = Integer.parseInt(mensajes[1]);
                String nombreUsuario = mensajes[2];
                
                switch(instruccion){
                
                    case Modelo.actualizarContadorSubasta:
                        System.out.println("Nombre de usuario = " + nombreUsuario + " codigo Articulo =" + codigo);
                        solicitarCambioParticipantes(nombreUsuario,codigo); 
                        break;
                        
                    case Modelo.actualizarOfertas:
                        System.out.println("Instruccion = "+ instruccion +" Nombre de usuario = " + nombreUsuario + " codigo Subasta =" + codigo);
                        solicitarCambioOfertas(nombreUsuario,codigo);
                        break;
                    
                    case Modelo.finalizarSubasta:
                        
                        finalizarSubasta(nombreUsuario, codigo);
                        
                        
                        break;
                }

            }           
        } catch (IOException ex) {
            System.out.println("hiloInformado error ioExeption");
        } 

    }
    
  
    
/**
 *  Descripción: Método que recibe un nombre de cliente(String) y un código de subasta(int).
 *               Este metodo actualiza constantemente la lista de ofertantes de un articulo.
 *               Recibe una lista de ofertas y se la envía al metodo actualizarListaOfertas en la
 *               Interfaz del cliente.               
 *  Parámetros de entrada:Un dato de tipo String y un dato de tipo int.
 *  Parámetros de salida: Ninguno.
 */       
    private void solicitarCambioOfertas(String nombre,int codigoSubasta)
    {
        String codigo = Integer.toString(codigoSubasta);
        if(interfazCliente.panelSubasta.lCodigoSubasta.getText().equals(codigo))
        {
            System.out.println(nombre + " = " + modelo.getNombreCliente() + "resultado " + nombre.equals(modelo.getNombreCliente()) );
            if(nombre.equals(modelo.getNombreCliente()))
            {
                System.out.println("son iguales no tendria que actualizar");
            }
            else
            {
                System.out.println("el usuario " + modelo.getNombreCliente() + "tendria que actualizar listaOfertas");
                interfazCliente.panelSubasta.actualizarListaOfertas(modelo.actualizarOfertas(codigoSubasta));
            }
        }    
    }
    
    
    
    
/**
 *  Descripción: Método encargado de recibir un dato de tipo String que representa el nombre de un
 *               cliente y un dato de tipo int que representa el código de un articulo.
 *               Este método actualiza el número de participantes que están ofertando para un articulo, sí la 
 *               cantidad de ofertantes es mayor a cuatro, da inicio a la subasta y habilita el botón Ofertar.
 *  Parámetros de entrada: Un dato de tipo String y un dato de tipo int.
 *  Parámetros de salida: Ninguno.
 */   
    private void solicitarCambioParticipantes(String nombre,int codigoArticulo)
    {
        String codigo = Integer.toString(codigoArticulo);
        if(interfazCliente.panelSubasta.lCodigoArticulo.getText().equals(codigo))
        {
            System.out.println(nombre + " = " + modelo.getNombreCliente() + "resultado " + nombre.equals(modelo.getNombreCliente()) );
            if(!nombre.equals(modelo.getNombreCliente()))
            {
                interfazCliente.panelSubasta.actualizarListaOfertas(modelo.actualizarOfertas(Integer.parseInt(interfazCliente.panelSubasta.lCodigoSubasta.getText())));
                interfazCliente.pack();
            }
            int cantidadParticipantes = modelo.actualizarContadorSubasta(codigoArticulo);
            interfazCliente.panelSubasta.lNumParticipantes.setText(Integer.toString(cantidadParticipantes));
            if(cantidadParticipantes == 4)
            {
                Subasta subastaActual = modelo.solicitarSubasta(codigoArticulo);

                interfazCliente.panelSubasta.bOfertar.setEnabled(true);
                interfazCliente.panelSubasta.lHora.setText(subastaActual.getHoraInicio().toString());

                long TiempoDuracion = modelo.solicitarTiempoDuracionSubasta();
                long tiempoFinalizacion = TiempoDuracion + subastaActual.getHoraInicio().getTime();
                Time horaFinalizacion = new Time(tiempoFinalizacion);
                JOptionPane.showMessageDialog(interfazCliente, "La subasta acaba de Empezar, tienes  " + TiempoDuracion/60000 + " minutos." + " La subasta se acaba a las : " + horaFinalizacion.toString());
            }
        }    
    
    }
    
    
    
    
/**
 *  Descripción: Método que una vez se es llamado, cierra el panel para subasta de un articulo, habilita el panel
 *               que muestra la lista de articulos en el panel, y muestra un mensaje avisando que acabó
 *               la subasta, muestra tambien el nombre del cliente que hizo la oferta mas grande.
 *  Parámetros de entrada: Un dato de tipo String que representa el nombre del cliente que mas ofertó y
 *                         el código del articulo que se estaba subastando.
 *  Parámetros de salida: Ninguno.
 */  
    private void finalizarSubasta(String nombre, int codigoArticulo)
    {
        String codigo = Integer.toString(codigoArticulo);
        if(interfazCliente.panelSubasta.lCodigoArticulo.getText().equals(codigo))
        {
            System.out.println(nombre + " = " + modelo.getNombreCliente() + "resultado " + nombre.equals(modelo.getNombreCliente()) );
            if(nombre.equals(modelo.getNombreCliente()))
            {
                int codigoSubasta = Integer.parseInt(interfazCliente.panelSubasta.lCodigoSubasta.getText());
                String NombreArticulo = interfazCliente.panelSubasta.lNombreArticulo.getText();
                int valor = modelo.actualizarOfertas(codigoSubasta).get(0).getValor();
                String nombreCliente = modelo.getNombreCliente();
                String textoFactura = CrearTextoFactura(codigoSubasta, NombreArticulo, valor, nombreCliente);
                modelo.guardarFactura(textoFactura, codigoSubasta);
                JOptionPane.showMessageDialog(interfazCliente, "El articulo es tuyo Felicidades : " + nombre +".\n" + textoFactura);
                Subasta actual = modelo.solicitarSubasta(codigoArticulo);

                
            }
            else
            {
                JOptionPane.showMessageDialog(interfazCliente,"finalizo la subasta, el mayor ofertante fue : " + nombre);
                interfazCliente.actualizarArticulos(Controlador.getInstace(), modelo.perdirArticulos(interfazCliente.cbCategoria.getSelectedItem().toString()));
                interfazCliente.cambiarAListaArticulos();
                interfazCliente.pack(); 
            }
        }           
    }
    
    public String CrearTextoFactura(int codigoSubasta,String NombreArticulo,int valor, String NombreCliente)
    {
        String mensajeFactura = "*************************************\n" + "Factura de compra # " + codigoSubasta + 
        "\nArticulo :" + NombreArticulo + 
        "\nValor de compra : " + valor + " $"+
        "\nA nombre de :" + NombreCliente + "\n" +
        "*************************************\n" +
        "Subasta PI siempre una buena opciòn";
        return mensajeFactura;
    }
            
}
