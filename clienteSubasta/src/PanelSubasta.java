
/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: PanelSubasta.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias
import java.applet.AudioClip;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

//Instanciamiento de las variables.
public class PanelSubasta  extends JPanel
{
    
    JLabel lImagen, lIconoParticipantes, lNumParticipantes, lIconoHora, lHora, lSubasta,lCodigoSubasta,lNombreArticulo,lPrecioBase, lCodigoArticulo;
    JButton bOfertar, bsalir;
    JTextField tfingresaoferta;
    JPanel panelSuperiorIzq, panelSuperiorDer, panelInferior, panelImagen, panelHora, panelCodigoSubasta,
           panelIngresoOferta, panelBotones, paneltfingresaoferta, panelArriba, panelAbajo;
    DefaultTableModel modeloTabla;
    JTable tabla;
    JScrollPane barras;
    AudioClip sonidoActualizar;
    
    //Constructor de la clase.
    public PanelSubasta()
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBackground(new Color(206,206,206));
        this.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        
        
        //Labels de información.
        lImagen = new JLabel(iconResize("Imagenes/prueba.jpg", 13, 13));
        lImagen.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        lIconoParticipantes = new JLabel(iconResize("Imagenes/person.png",90, 90));
        lIconoHora = new JLabel(iconResize("Imagenes/clock.png",100, 100));
        lHora = new JLabel("00:00:00");
        lNumParticipantes = new JLabel("X");
        lSubasta = new JLabel("Código de subasta: ");
        lCodigoSubasta = new JLabel("0000222");
        lNombreArticulo = new JLabel("honda centra");
        lCodigoArticulo = new JLabel("CodigoArticulo");
        lPrecioBase = new JLabel("50000 $");
        
        
        //Botones
        bOfertar = new JButton("Ofertar");
        bsalir = new JButton("Salir");
        
        //Ingreso de valores.
        tfingresaoferta = new JTextField();
       
        panelImagen = new JPanel();
        panelImagen.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelImagen.add(lImagen);
        
        panelHora = new JPanel();
        panelHora.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelHora.add(lIconoParticipantes);
        panelHora.add(lNumParticipantes);
        panelHora.add(lIconoHora);
        panelHora.add(lHora);
        
        panelCodigoSubasta = new JPanel();
        panelCodigoSubasta.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelCodigoSubasta.add(lSubasta);
        panelCodigoSubasta.add(lCodigoSubasta);
       
        panelSuperiorIzq = new JPanel();
        panelSuperiorIzq.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelSuperiorIzq.setLayout(new BoxLayout(panelSuperiorIzq, BoxLayout.Y_AXIS));
        panelSuperiorIzq.add(panelImagen);
        panelSuperiorIzq.add(panelHora);
        
        paneltfingresaoferta = new JPanel();
        paneltfingresaoferta.add(tfingresaoferta);
        
        panelBotones = new JPanel();
        panelBotones.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelBotones.add(bOfertar);
        panelBotones.add(bsalir);
        
        panelSuperiorDer = new JPanel();
        panelSuperiorDer.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelSuperiorDer.setLayout(new BoxLayout(panelSuperiorDer, BoxLayout.Y_AXIS));
        panelSuperiorDer.add(lNombreArticulo);
        panelSuperiorDer.add(lCodigoArticulo);
        panelSuperiorDer.add(lPrecioBase);
        panelSuperiorDer.add(panelCodigoSubasta);
        panelSuperiorDer.add(tfingresaoferta);
        panelSuperiorDer.add(panelBotones);
  
        sonidoActualizar =  java.applet.Applet.newAudioClip(getClass().getResource("Sonidos/Actualizacion.wav"));
        //Creación de la tabla.
        modeloTabla = new DefaultTableModel();
        int columna = 4;
        Object[] nombCol = new Object[columna];
        nombCol[0] = "Código";
        nombCol[1] = "Oferta";
        nombCol[2] = "Tiempo";
        nombCol[3] = "Usuario";
        modeloTabla.setColumnIdentifiers(nombCol);
        Object [] datosFila = new Object[columna];
   
                datosFila[1] = "Hola";
                datosFila[0] = "primera";
                modeloTabla.addRow(datosFila);
   
       
        
        tabla = new JTable(modeloTabla);
        barras = new JScrollPane(tabla);
        panelInferior = new JPanel();
        panelInferior.add(barras);
        
        panelArriba = new JPanel();
        panelArriba.setBorder(BorderFactory.createLoweredBevelBorder());
        panelAbajo = new JPanel();
        panelAbajo.setBorder(BorderFactory.createLoweredBevelBorder());
        panelArriba.add(panelSuperiorIzq);
        panelArriba.add(panelSuperiorDer);
        panelAbajo.add(barras);
        
        this.add(panelArriba);
        this.add(panelAbajo);
        setVisible(true);
        
        
        
     }        
    
   /**
 *  Descripción: Método encargado de recibir un ArrayList de objetos que contine los datos necesarios
 *               para que cada vez que se sea llamado, actualice los datos del articulo que se está subastando.
 *               Se condiciona que si para un articulo no hay mas de cuatro ofertantes, no inicie la subasta,
 *               esto se logra deshabilitando el botón ofertar.   
 *               En caso de se cumpla la condición, se habilitara el botón de ofertar y se imprimiraen pantalla
 *               la hora de inicio de la subasta.
 *  Parámetros de entrada: Un ArrayList <object> con los datos necesarios para la actualización de la subasta.
 *  Parámetros de salida: Ninguno.
 */    
    public void actualizarSubasta(ArrayList<Object> informacion)
    {
        Subasta subasta = (Subasta)informacion.get(0);
        Articulo articulo = (Articulo)informacion.get(1);
        ArrayList<Oferta> listaOfertas = (ArrayList<Oferta>)informacion.get(2);
        lImagen.setIcon(iconResize(articulo.getUrlImagen(), 13, 13)); 
        lNombreArticulo.setText(articulo.getNombre());
        lCodigoArticulo.setText(Integer.toString(articulo.getCodigo()));
        lPrecioBase.setText(Integer.toString(articulo.getPrecioBase()));
        lCodigoSubasta.setText(Integer.toString(subasta.getCodigo()));
        lNumParticipantes.setText(Integer.toString(subasta.getParticipantes()));


        if(subasta.getParticipantes() < 4)
        {
            listaOfertas.clear();
            actualizarListaOfertas(listaOfertas);
            bOfertar.setEnabled(false);        
        }

        else
        {
           lHora.setText(""+subasta.getHoraInicio());
           actualizarListaOfertas(listaOfertas);
           bOfertar.setEnabled(true);
        }

    }
/**
 *  Descripción: Método encaragdo de recibir una lista de ofertas e insertar los valores en la tabla que mostrará al
 *               cliente en tiempo real las ofertas que se realizan al articulo que se está subastando.
 *  Parámetros de entrada: Un ArrayList de objetos de tipo Ofertas.
 *  Parámetros de salida: Ninguno.
 */    
    public void actualizarListaOfertas(ArrayList<Oferta> listaOfertas)
    {
        sonidoActualizar.play();
        modeloTabla.setRowCount(0);
        System.out.println("entro");
        for(Oferta actual : listaOfertas)
        {
               Object [] datosFila = new Object[modeloTabla.getColumnCount()];
                datosFila[0] = actual.getCodigo();
                if(actual.getValor() == 0)
                    datosFila[1] = "Ingreso";
                else
                    datosFila[1] = actual.getValor();

                datosFila[2] = actual.getHoraOferta();
                datosFila[3] = actual.getNombreCliente();
                modeloTabla.addRow(datosFila);        
        }

    
    }
    
    
     public ImageIcon iconResize(String url, int width, int height )
    {
          ImageIcon icon; 
          icon = new ImageIcon(getClass().getResource(url));
          Image img = icon.getImage() ;  
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          Image newimg = img.getScaledInstance( screenSize.width/width, screenSize.width/height,  java.awt.Image.SCALE_AREA_AVERAGING) ; 
          icon.setImage(newimg);
          return icon;

    }
     
/**
 *  Descripción: Método encargado de aignarle las señale s de escucha al botón ofertar.
 *  Parámetro de entrada: Ninguno.
 *  Parámetro de salida: ninguno.
 *  
 */ 
    public void asignarEscuchas(Controlador cont)
    {
        bOfertar.addActionListener(cont);
    }
     
     
     
}