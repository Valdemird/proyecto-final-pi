/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: Subasta.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/

//Importe de librerias
import java.io.Serializable;
import java.sql.Time;

//Instanciamiento de las variables.
public class Subasta implements Serializable{
    static final long serialVersionUID = 9L;
    private int codigo;
    private Time horaInicio;
    private int participantes;
    private int codigoArticulo;

 /**
 *  Descripción: Método encargado de retornar un dato de tipo int que contiene la variable "codigo" que
 *              representa el código de una subasta.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo int.
 */    
    public int getCodigo() 
    {
        return codigo;
    }

    
    
    
/**
 *  Descripción: Método encargado de recibir un dato de tipo int e insertarlo la variable "codigo" que
 *              representa el código de una subasta.
 *  Parámetros de entrada: Undato de tipo int.
 *  Parámetros de salida: Ninguno.
 */    
    
    public void setCodigo(int codigo) 
    {
        this.codigo = codigo;
    }

    
    
    
    
/**
 *  Descripción: Método encargado de retornar un dato de tipo Time que contiene la variable "horaInicio" que
 *              representa la hora de inicio de una subasta.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo Time.
 */    
    
    public Time getHoraInicio() 
    {
        return horaInicio;
    }

    
    
    
/**
 *  Descripción: Método encargado de recibir un dato de tipo Time e insertarlo la variable "horaInicio" que
 *              representa la hora de inicio de una subasta.
 *  Parámetros de entrada: Un dato de tipo Time.
 *  Parámetros de salida: Ninguno.
 */    
    
    public void setHoraInicio(Time horaInicio) 
    {
        this.horaInicio = horaInicio;
    }

    
    
/**
 *  Descripción: Método encargado de retornar un dato de tipo int que contiene la variable "participantes" que
 *              representa el número de participantes de una subasta.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo int.
 */    

    public int getParticipantes() 
    {
        return participantes;
    }

    
    
    
/**
 *  Descrpción: Método encargado de recibir un dato de tipo int e insertarlo la variable "participantes" que
 *              representa el número de participantes de una subasta.
 *  Parámetros de entrada: Un dato de tipo int.
 *  Parámetros de salida: Ninguno.
 */    
    
    public void setParticipantes(int participantes) 
    {
        this.participantes = participantes;
    }

    
    
    
/**
 *  Descrpción: Método encargado de retornar un dato de tipo int que contiene la variable "codigoArticulo" que
 *              representa el código del articulo que se está subastando.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo int.
 */    
    
    public int getCodigoArticulo() {
        return codigoArticulo;
    }

    
    
    
    
/**
 *  Descrpción: Método encargado de recibir un dato de tipo int e insertarlo la variable "codigoArticulo" que
 *              representa el código del articulo que se está subastando.
 *  Parámetros de entrada: Un dato de tipo int.
 *  Parámetros de salida: Ninguno.
 */    
    
    public void setCodigoArticulo(int codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }
    
    
    
}




