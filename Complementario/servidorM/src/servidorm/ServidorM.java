/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorm;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class ServidorM {

    /**
     * @param args the command line arguments
     */
    

    public static void main(String[] args) {
       
        try {
            MulticastSocket conexion = new MulticastSocket();
            DatagramPacket data;
            byte[] b;
            b = horaActual().getBytes();
           
            
            data  = new DatagramPacket(b, b.length,InetAddress.getByName("235.1.1.1"), 4000);
            
            while(true)
            {
                data.setData(horaActual().getBytes());
                conexion.send(data);
                System.out.println("Enviando " + b.length );
                System.out.println(new String(data.getData()));
                Thread.sleep(1000);
            
            }
        } catch (IOException ex) {
            Logger.getLogger(ServidorM.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch(InterruptedException ex)
        {
            System.out.println("servidorm.ServidorM.main()");
        }
    }
    public static String horaActual()
    {
        Calendar  fecha = Calendar.getInstance();
        String informacion;
        informacion = Integer.toString(fecha.get(Calendar.HOUR_OF_DAY)) + " - "
        + Integer.toString(fecha.get(Calendar.MINUTE)) + " - "
        + Integer.toString(fecha.get(Calendar.SECOND));
        return informacion;
    
    }
    
}
