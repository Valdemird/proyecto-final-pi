
/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: Oferta.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

//Instanciamiento de las variables.
public class Oferta implements Serializable{
    static final long serialVersionUID = 8L;
    private int valor;
    private int codigoSubasta;
    private String nombreCliente;
    private int codigo;
    private Time horaOferta;

    
    
    //Constructor de la clase.
    public Oferta(int codigoSubasta, String nombreCliente, int valor) {
        this.valor = valor;
        this.codigoSubasta = codigoSubasta;
        this.nombreCliente = nombreCliente;
    }
    
    
/**
 *  Descripción: Método encargado de retornar un dato de tipo int que contiene la variable valor.
 *               Este valor representa el valor de una oferta realizada a un articulo.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo int.
 */
    public int getValor() 
    {
        return valor;
    }

    
    
    
    /**
 *  Descripción: Método encargado de recibir un dato de tipo int e insertarlo en la variable "valor".
 *               Este valor representa el valor de una oferta realizada a un articulo.
 *  Parámetros de entrada:
 *  Parámetros de salida:
 */

    public void setValor(int valor) {
        this.valor = valor;
    }

    
    
    
/**
 *  Descripción: Método encargado de retornar un dato de tipo int que contiene la variable "codigoSubasta" y que
 *               representa el código que representa una subasta en especifíco.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo int.
 */
    
    public int getCodigoSubasta() {
        return codigoSubasta;
    }

    
    
    
/**
 *  Descripción: Método encargado de recibir un dato de tipo int e insertarlo a la variable "codigoSubasta", que
 *               representa el código que representa una subasta en especifíco.
 *  Parámetros de entrada:
 *  Parámetros de salida:
 */
    
    public void setCodigoSubasta(int codigoSubasta) {
        this.codigoSubasta = codigoSubasta;
    }

    
    
    
    
/**
 *  Descripción: Método encargado de retornar un dato de tipo String que contiene la variable "nombre" 
 *               que representa el nombre del cliente que realizó una oferta
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo String.
 */
    
    public String getNombreCliente() {
        return nombreCliente;
    }

    
    
    
 
/**
 *  Descripción: Método encargado de recibir un dato de tipo String e insertarlo en la variable "nombre" 
 *               que representa el nombre del cliente que realizó una oferta
 *  Parámetros de entrada: Un dato de tipo String.
 *  Parámetros de salida: Ninguno.
 */
    
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    
    
    
/**
 *  Descripción: Método encargado de retornar un dato de int que contiene la variable "codigo" que representa
 *               el código de un articulo.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo int.
 */
    
    public int getCodigo() {
        return codigo;
    }

    
    
    
/**
 *  Descripción: Método encargado de recibir un dato de int e insertarlo la variable "codigo" que representa
 *               el código de un articulo.
 *  Parámetros de entrada:
 *  Parámetros de salida:
 */
    
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    
    
    
/**
 *  Descripción: Método encargadoo de retorna un dato de tipo Time que contiene la variable horaOferta, que
 *               representa el dato hora en que se realizó una oferta.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Un dato de tipo Time.
 */
    
    public Time getHoraOferta() {
        return horaOferta;
    }

    
    
    
    
    
/**
 *  Descripción: Método encargadoo de recibir un dato de tipo Time e insertarlo en la variable horaOferta, que
 *               representa el dato hora en que se realizó una oferta.
 *  Parámetros de entrada: Un dato de tipo Time.
 *  Parámetros de salida: Ninguno.
 */
    
    public void setHoraOferta(Time horaOferta) {
        this.horaOferta = horaOferta;
    }
    
}