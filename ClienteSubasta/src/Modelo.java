

/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: Modelo.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias.
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;





//Instanciamiento de las variables.
public class Modelo 
{
    static Modelo instance;
    static final String pedirArticulos = "pedirArticulos";
    static final String pedircategorias = "pedirCategorias"; 
    static final String pedirSaldoCliente = "pedirSaldoCliente";
    static final String ingresarCliente = "ingresarCliente";
    static final String ingresarSubasta = "ingresarSubasta";
    static final String generarOferta = "generarOferta";
    static final String actualizarOfertas = "actualizarOfertas";
    static final String actualizarContadorSubasta = "actualizarContadorSubasta";
    static final String solicitarSubasta = "solicitarSubasta";
    static final String solicitarDuracion = "solicitarDuracion";
    static final String finalizarSubasta = "fin";
    
    private String nombreCliente;
    Socket conexion;
    ObjectInputStream entrada;
    ObjectOutputStream salida;

    
    /**
     * Constructor de la clase.
     * Aquí se crea el Hilo individual para conectarse con el servidor.
     * Estará constantemente buscando conexión con el servidor, una vez establecida la conexión,
     * estará cnectado hasta que el cliente cierre el programa, cuando esto suceda se cierra la
     * conexión.
     */
    public Modelo() {
        
        if(instance == null)
        {
            System.out.println("Buscando Servidor");
            while(conexion == null)
            {

                try {
                    conexion = new Socket("127.0.0.1", 12345);
                    System.out.print("Servidor Encontrado");
                    entrada = new ObjectInputStream(conexion.getInputStream());
                    salida = new ObjectOutputStream(conexion.getOutputStream());
                    salida.flush();
                } catch (IOException ex) {
                    System.out.print(".");
                }               
            }
            instance = this;
        }
        
        System.out.println("ya existe el objeto");
    }
    
    public static Modelo getInstace()
    {
        if(instance == null)
        {
           instance = new Modelo();
        }
        return instance;
    
    }
    
    
    
    /**
     * Descrpción: Método encargado de enviar al servidor una solicitud, en este caso le pide los articulos,
     *             que necesite según la categoría. El servidor le retornará un arreglo de objetos y este metodo
     *             los retornará como un arrayList de Articulos en donde sea llamado.
     * Parámetros de entrada: Ninguno.
     * Parámetros de salida: Un ArrayList de Articulos.
     * @return 
     */
    public synchronized ArrayList<Articulo> perdirArticulos(String categoriaActual) 
    {
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        ArrayList<Articulo> mensajeEntrada = new ArrayList<Articulo>();
        mensajeSalida.add(pedirArticulos);
        mensajeSalida.add(categoriaActual);
        try {
            salida.writeObject(mensajeSalida);
            salida.flush();
            //salida.reset();
            
            mensajeEntrada = (ArrayList<Articulo>)  entrada.readObject();
            System.out.println("articulos recibidos");
        } catch (IOException ex) {
            System.out.println("Error entrada salida");
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            System.out.println("Error entrada salida");
        }
      return mensajeEntrada;  
    }   
    
    
    
    
     /**
     * Descrpción: Método encargado de enviar al servidor una solicitud, en este caso le pide las categorias que 
     *             existen en el servidor. El servidor le retornará un arreglo de objetos y este metodo
     *             los retornará como un arrayList de String en donde sea llamado.
     * Parámetros de entrada: Ninguno.
     * Parámetros de salida: Un ArrayList de String.
     * @return 
     */
    public synchronized ArrayList<String> pedirCategorias() 
    {
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        ArrayList<String> mensajeEntrada = new ArrayList<String>();
        mensajeSalida.add(pedircategorias);
        try 
        {
          salida.writeObject(mensajeSalida);
          salida.flush();
          mensajeEntrada = (ArrayList<String>) entrada.readObject();
           System.out.println("hola: " + mensajeEntrada);

        } catch (IOException e)
        {
            System.out.println("Error entrada salida categoria"); 
            e.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Error metodo");
        }    
        return mensajeEntrada;
    }
    
    
    
    
    public synchronized int pedirSaldoCliente()
    {
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        int mensajeEntrada = 0;
        mensajeSalida.add(pedirSaldoCliente);
        mensajeSalida.add(nombreCliente);
    
        
        try 
        {
          salida.writeObject(mensajeSalida);
          salida.flush();
          mensajeEntrada = (Integer) entrada.readObject();
            System.out.println(mensajeEntrada);

        } catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Error metodo");
        }    
        return mensajeEntrada;
    }
    
    
    
    
/**
 *  Descripción:
 *  Parámetros de entrada:
 *  Parámetros de salida:
 * 
 *  
 */
    public synchronized ArrayList<Object> IngresarSubasta(int codigoArticulo)
    {
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        ArrayList<Object> mensajeEntrada = new ArrayList<Object>();
        mensajeSalida.add(ingresarSubasta);
        mensajeSalida.add(codigoArticulo);
        mensajeSalida.add(nombreCliente);
        try 
        {
          salida.writeObject(mensajeSalida);
          salida.flush();
          mensajeEntrada = (ArrayList<Object>) entrada.readObject();

        } catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Error metodo");
        }    
        return mensajeEntrada;
    }
    
    
    
/**
 *  Descripción: Método que se encarga de envíar al servidor una oferta generada por el cliente
 *               a un objeto subastado. Una vez enviada la oferta, espera a que le retorne un dato de tipo boolean
 *               el cual representa si el servidor realizó el cambio o no. De haberlo realizado el servidor
 *               tambien le retorna una lista de ofertas para ser actualizadas en el panel de subasta de un articulo.
 *               En este caso se usa un ArrayList de objetos el cual en la posición cero trae el dato booleano y en la 
 *               posición 1 el ArrayList de ofertas.
 *  Parámetros de entrada: Un dato de tipo int que representa el valor de la oferta y otro dato de tipo int
 *                         que representa el codigo de la subasta donde se está subastando el articulo. 
 *  Parámetros de salida: Un ArrayList de objetos de tipo Oferta.
 */    
    public synchronized ArrayList<Oferta> generarOferta(int valor, int codigoSubasta)
    {
        Oferta solicitudOferta = new Oferta(codigoSubasta, nombreCliente, valor);
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        ArrayList<Object> mensajeEntrada = new ArrayList<Object>();;
        mensajeSalida.add(generarOferta);
        mensajeSalida.add(solicitudOferta);
        try 
        {
            salida.writeObject(mensajeSalida);
            salida.flush();
            mensajeEntrada = (ArrayList<Object>) entrada.readObject();
            boolean ofertarealizada = (boolean)mensajeEntrada.get(0);;
            ArrayList<Oferta>  listaOfertas = null;
            if(ofertarealizada)
            {
                listaOfertas = (ArrayList<Oferta>)mensajeEntrada.get(1);         
            }
            return listaOfertas;
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Error metodo");
        }
        return null;
    }
    
    
    
/**
 *  Descripción:Método que cada vez que se es llamado, envía una petición al servidor de para que le envíe una
 *              lista de ofertas, esto con la finalidad de estar actualizando el panel en donde se muestra las 
 *              ofertas que recibe el articulo mientras se es subastado.
 *  Parámetros de entrada: Un dato de tipo entero que representa el código de la subasta.
 *  Parámetros de salida: Un ArrayLIst de tipo Oferta.
 */       
    public synchronized ArrayList<Oferta> actualizarOfertas(int codigoSubasta)
    {
       
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        ArrayList<Oferta> mensajeEntrada = new ArrayList<Oferta>();
        mensajeSalida.add(actualizarOfertas);
        mensajeSalida.add(codigoSubasta);
        try 
        {
            salida.writeObject(mensajeSalida);
            salida.flush();
            mensajeEntrada = (ArrayList<Oferta>) entrada.readObject();
            return mensajeEntrada;          
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Error metodo");
        }
        return null;
    
    }
    
    
    
    
/**
 *  Descripción: Metodo que se encarga de envíar una petición al servidor para que este le retorne el 
 *               número de personas que se encuentran en la subasta del mismo articulo.
 *  Parámetros de entrada: Un dato de tipo int que representa el códgio del articulo que se está subastando.
 *  Parámetros de salida: Un dato de tipo int que representa el número de ofertates del articulo que se está subastando.
 */      
    public int actualizarContadorSubasta(int codigoArticulo)
    {
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        int mensajeEntrada = -1;
        mensajeSalida.add(actualizarContadorSubasta);
        mensajeSalida.add(codigoArticulo);
        try 
        {
            salida.writeObject(mensajeSalida);
            salida.flush();
            mensajeEntrada = (int) entrada.readObject();
            return mensajeEntrada;          
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Error metodo");
        }
        return mensajeEntrada;
    
    }
    
    public Subasta solicitarSubasta(int codigoArticulo)
    {
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        Subasta mensajeEntrada = null;
        mensajeSalida.add(solicitarSubasta);
        mensajeSalida.add(codigoArticulo);
        try 
        {
            salida.writeObject(mensajeSalida);
            salida.flush();
            mensajeEntrada = (Subasta) entrada.readObject();
            return mensajeEntrada;          
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Error metodo");
        }
        return mensajeEntrada;
    
    }
    
    public long solicitarTiempoDuracionSubasta()
    {
        ArrayList<Object> mensajeSalida = new ArrayList<Object>();
        mensajeSalida.add(solicitarDuracion);
        long mensajeEntrada = 0;
        try 
        {

            salida.writeObject(mensajeSalida);
            salida.flush();
            mensajeEntrada = (long) entrada.readObject();
            return mensajeEntrada;          
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Error metodo");
        }
        return mensajeEntrada;
    }
    
        public void guardarFactura(String mensaje, int codigoSubasta) 
    {
        try {
            File archivo = new File("Facturas/factura # "+ codigoSubasta + ".txt");
            BufferedWriter buffer = new BufferedWriter(new FileWriter(archivo));
            buffer.write(mensaje);
            buffer.flush();
            buffer.close();
        } catch (FileNotFoundException e) {
            System.err.print("error al crear el archivo");
        }
        catch(IOException e)
        {
            System.err.print("error al guardar el archivo");
        }
    }

    
    
/**
 *  Descripción:
 *  Parámetros de entrada:
 *  Parámetros de salida:
 */      
    public synchronized String getNombreCliente() {
        return nombreCliente;
    }

    
    
/**
 *  Descripción:
 *  Parámetros de entrada:
 *  Parámetros de salida:
 */      
    public synchronized void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }
    
       
    
    
    
}

