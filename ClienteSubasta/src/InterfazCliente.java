
/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: InterfazCliente.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


// Importe de librerias
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.MenuBar;
import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.StyleConstants;



//Instanciamiento de las variables.
public class InterfazCliente extends JFrame
{
    
    PanelSubasta panelSubasta;
    ArrayList<PanelArticulo> articulos;
    JLabel lHora, lNombre, lHoraServidor;
    JComboBox cbCategoria;
    JScrollPane lArticulos;
    Container contenedor;
    JPanel panelSuperior,panelCentro, panelInferior, panelHora, panelDatoUsuario, panelcbCategorias, panelAreaArticulos,
           panelNavegacion;
    JButton bRegresar;
    
    
    //Constructor de la clase.
    public InterfazCliente ()
    {
        //Generando el contenedor principal.
        super("Subasta PI");
        contenedor = getContentPane();
        
        //Creando el Arraylist de articulos.
        articulos = new ArrayList<PanelArticulo>();
        
        
        //Labels de información.
        lHora = new JLabel("Hora");
        lNombre = new JLabel("Guillermo hernadez");
        lHoraServidor = new JLabel("Hora Servidor:");
        
        //Combobox con las categorias que existen en la subasta.
        cbCategoria = new JComboBox<String>();
        
        //Boton de regreso a la ventana anterior
        bRegresar = new JButton("Regresar");
        
        
        //Creacion de paneles
        panelSuperior = new JPanel();
        panelSuperior.setBorder(BorderFactory.createEtchedBorder());
        panelCentro = new JPanel();
        panelInferior = new JPanel();
        panelHora = new JPanel();
        panelHora.add(lHoraServidor);
        panelHora.add(lHora);
        panelDatoUsuario = new JPanel();
        panelDatoUsuario.add(lNombre);
        panelcbCategorias = new JPanel();
        panelcbCategorias.add(cbCategoria);
        panelSubasta = new PanelSubasta();
        panelAreaArticulos = new JPanel();
        panelNavegacion = new JPanel();
        
        panelAreaArticulos.setLayout(new GridLayout(0,3,2,2));
        lArticulos = new JScrollPane(panelAreaArticulos);
        
        panelcbCategorias.setLayout(new FlowLayout());
        panelSuperior.setBorder(BorderFactory.createEtchedBorder(Color.lightGray, Color.white));
        panelSuperior.setLayout(new BorderLayout());
        panelSuperior.add(panelHora, BorderLayout.WEST);
        panelSuperior.add(panelDatoUsuario, BorderLayout.CENTER);
        panelSuperior.add(panelcbCategorias, BorderLayout.EAST);
        panelCentro.setBorder(BorderFactory.createEtchedBorder(Color.lightGray, Color.white));
        panelCentro.add(lArticulos);
        panelInferior.add(bRegresar);
        bRegresar.setVisible(false);
        panelNavegacion.setLayout((new BorderLayout()));
        panelNavegacion.add(panelSuperior, BorderLayout.NORTH);
        panelNavegacion.add(panelCentro, BorderLayout.CENTER);
        panelNavegacion.add(panelInferior, BorderLayout.SOUTH);
        
        contenedor.add(panelNavegacion); 
        lNombre.setIcon(panelSubasta.iconResize("Imagenes/person.png", 90, 90));
        lHoraServidor.setIcon(panelSubasta.iconResize("Imagenes/clock.png", 90, 90));
        setIconImage(panelSubasta.iconResize("Imagenes/iconoSubasta.png", 1, 1).getImage());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        pack();
        setVisible(false);
        
        
        
    }        
    

  
/**
 *  Descripción: Metodo encargado de asignar las señales de escucha a los botones y a los Combobox que están presentes
 *               en esta interfaz.
 *  Parámetro de entrada: Un objeto de tipo Controlador.
 *  Parámetro de salida: Ninguno.
 *  
 */        
    public void asignarEscuchas(Controlador cont)
    {
        cbCategoria.addActionListener(cont);
        bRegresar.addActionListener(cont);
    }
    
    
    
    
/**
 *  Descripción: Método encargado de actualizar en la interfaz, el listado de articulos cada vez que se
 *               es llamado. Este método limpia el panel destinado para desplegar la lista de articulos
 *               e inserta el listado de articulos solicitado.
 *  Parámetros de entrada: Un objeto de tipo Controlador y un ArrayList de objetos de tipo Articulo.
 *  Parámetros de salida: Ninguno.
 * 
 *   
 */     
    public void actualizarArticulos(Controlador control, ArrayList<Articulo> artModelo)
    {
        if(!articulos.isEmpty())
        {
            articulos.clear();
            panelAreaArticulos.removeAll();
        }
        
        for(int i = 0 ; i<artModelo.size(); i++)
        {
            if(artModelo.get(i).getPrecioFinal() == 0)
            {
                PanelArticulo actual = new PanelArticulo(control);
                actual.lCodigo.setText(Integer.toString(artModelo.get(i).getCodigo()));
                actual.tDescripcion.setText(artModelo.get(i).getDescripcion() );
                actual.lImagenArticulo.setIcon(actual.iconResize(artModelo.get(i).getUrlImagen(), 15, 15)); 
                actual.lPrecioBase.setText(Integer.toString(artModelo.get(i).getPrecioBase()));
                //actual.lPrecioFinal.setText("Precio final: "+Integer.toString(artModelo.get(i).getPrecioFinal()));
                actual.lNombreArticulo.setText(artModelo.get(i).getNombre());

                articulos.add(actual);
            }

        }
        
        
        for(PanelArticulo actual : articulos)
            panelAreaArticulos.add(actual);
        
        if(!this.isVisible())
            setVisible(true);
        pack(); 
    }

    
    
    
    
/**
 *  Descripción: Método encargado de recibir desde la clase modelo un ArrayList de String que representa cada
 *               una de las categorias y las inserta en el combobox destinado para tener esta categorias y ser
 *               seleccionadas por el cliente para mirar que articulos tiene cada una.
 *  Parámetros de entrada: Un ArrayList de Strings.
 *  Parámetros de salida: Ninguno.
 */    
    public void actualizarCategorias (ArrayList<String> catModelo)
    {
     if(cbCategoria.getItemCount() != 0)
         cbCategoria.removeAllItems();
     
     for(String actual : catModelo)
     {
         cbCategoria.addItem(actual);
     }
    }
    
 
    
/**
 *  Descripción: Método que se encarga de agregar a la interfaz el panel donde el cliente empezará a
 *               ofertar en una subasta de algún articulo.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Ninguno.
 */    
    public void cambiarASubasta()
    {
        cbCategoria.setVisible(false);
        bRegresar.setVisible(true);
        panelNavegacion.remove(panelCentro);
        panelNavegacion.add(panelSubasta, BorderLayout.CENTER);
        
        
    }
    
    
    
    
/**
 *  Descripción: Método encargado de agregar a la interfaz el panel donde el cliente visualiza la lista de 
 *               articulos a subastar. Por lo general este método se usa cuando termina una subasta o el 
 *               cliente se retira de una subasta.
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Ninguno.
 */    
    public void cambiarAListaArticulos()
    {
        cbCategoria.setVisible(true);
        bRegresar.setVisible(false);
       panelNavegacion.remove(panelSubasta);
       panelNavegacion.add(panelCentro, BorderLayout.CENTER);
    }
    
    
    
    
/**
 *  Descripción: Metodo que se encarga de retornar un dato de tipo entero que representa el codigo del
 *               articulo en el cual se clickeo el botón de ingreso a subasta.
 *  Parámetros de entrada: Un objeto de tipo JButton que representa el botón de ingreso a subasta de un articulo.
 *  Parámetros de salida: Un dato de tipo int que representa el código de un articulo.
 */    
    public int pedirCodigoPanel(JButton boton)
    {
        for(PanelArticulo actual : articulos)
        {
            if(boton.equals(actual.bAcceder))
            {
                int codigo = Integer.parseInt(actual.lCodigo.getText());
                return codigo;
            }
        
        }
        return -1;
    }
    
    
    
    
}
