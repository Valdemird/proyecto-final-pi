
/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: Controlador.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias.
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLXML;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;



//Instanciamiento de las variables.
public class Controlador  implements ActionListener
{
    static private Controlador instance;
    InterfazCliente interfazCliente;
    InterfazIngresoUsuario interfazIngreso;
    Modelo modelo;
    HiloInformado informador;
    
    //Constructor de la clase.
    public Controlador(InterfazCliente interfazCliente,InterfazIngresoUsuario interfazIngreso)
    {
        
        this.interfazCliente = interfazCliente;
        this.interfazIngreso = interfazIngreso;
        modelo = new Modelo();
        interfazCliente.asignarEscuchas(this);
        interfazIngreso.asignarEscuchas(this);
        interfazCliente.panelSubasta.asignarEscuchas(this);
        informador = new HiloInformado(interfazCliente, modelo);
        instance = this;
        
    }        
    
    
     /**
     * Descripción: Método encargado de recibir una señal(escucha) y de acuerdo a ella
     *              llevar a cabo el metodo asignado a esa escucha.
     * Parámetro de entrada: Una señal de un botón.
     * Parámetro de salida: Ninguno.
     */
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(interfazIngreso.bIngresar))
        {
           String nombre = interfazIngreso.tfIngresoUsuario.getText();
            modelo.setNombreCliente(nombre);
            if(modelo.pedirSaldoCliente() != -1)
            {
                interfazCliente.lNombre.setText(modelo.getNombreCliente());
                cerrarVentanaIngreso();
                interfazCliente.actualizarCategorias(modelo.pedirCategorias());
                interfazCliente.actualizarArticulos(this, modelo.perdirArticulos(interfazCliente.cbCategoria.getSelectedItem().toString()));
                mostraVentanaInterfaz();
                informador.start();
            }
            else
            {
                JOptionPane.showMessageDialog(interfazIngreso,"El usuario no existe");
            }

        }
        
        
        if(e.getSource().equals(interfazIngreso.bSalir))
        {
            System.exit(0);
        } 
        
        
        if(e.getSource().equals(interfazCliente.bRegresar))
        {
            interfazCliente.cambiarAListaArticulos();
            interfazCliente.actualizarArticulos(Controlador.getInstace(), modelo.perdirArticulos(interfazCliente.cbCategoria.getSelectedItem().toString()));
            interfazCliente.pack();
        } 
        
        
        if(e.getSource().equals(interfazCliente.cbCategoria))
        {
            interfazCliente.actualizarArticulos(this, modelo.perdirArticulos(interfazCliente.cbCategoria.getSelectedItem().toString()));
        }
        
        
        if(e.getSource() instanceof JButton)
        {
            JButton boton = (JButton)e.getSource();
            if(boton.getText().equals("Ingresar a Subasta"))
            {
                int codigoArticulo = interfazCliente.pedirCodigoPanel(boton);
                ArrayList<Object> mensaje = modelo.IngresarSubasta(codigoArticulo);
                interfazCliente.panelSubasta.actualizarSubasta(mensaje);
                interfazCliente.cambiarASubasta();
                interfazCliente.pack();
            }

        }
        
        if(e.getSource().equals(interfazCliente.panelSubasta.bOfertar))
        {

            PanelSubasta pSubasta = interfazCliente.panelSubasta;
            try {
                int valor = Integer.parseInt(pSubasta.tfingresaoferta.getText());   
                int codigoSubasta = Integer.parseInt(pSubasta.lCodigoSubasta.getText());
                ArrayList<Oferta> listaOfertas = modelo.generarOferta(valor, codigoSubasta);
                if(listaOfertas != null)
                {
                    pSubasta.actualizarListaOfertas(listaOfertas);
                    pSubasta.bOfertar.setText("");
                }
                else
                {
                   JOptionPane.showMessageDialog(interfazCliente, "oferta de menor valor que la ultima realizada");
                }
            } catch (NumberFormatException ex) {
                 JOptionPane.showMessageDialog(interfazCliente, "Por favor ingrese un numero");
            }

            

        
        }
    }
    
    
/**
 *  Descripción: Método que se encarga de cerrar la ventana de ingreso de usuario haciendola no visible
 *               cuando se es llamado.
 *  Parámetro de entrada: Ninguno.
 *  Parámetro de salida: Ninguno.
 *  
 */    
    public void cerrarVentanaIngreso()
    {
        interfazIngreso.setVisible(false);
    }
    
    
/**
 *  Descripción: Método que se encarga de hacer visible la ventana de interfaz para las operaciones
 *               que realizará el cliente en la subasta.
 *  Parámetro de entrada: Ninguno.
 *  Parámetro de salida: Ninguno.
 *  
 */     
    public void mostraVentanaInterfaz()
    {
        interfazCliente.setVisible(true);
    }
  
    
    
/**
 *  Descripción: Método que se encarga de retornar una instancia de la clase controlador.
 *  Parámetro de entrada: Ninguno.
 *  Parámetro de salida: Ninguno.
 *  
 */  
    static public Controlador getInstace()
    {
        return instance;
    }
          
}
    