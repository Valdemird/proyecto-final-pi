import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Container;
import java.awt.Dimension;

/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: PanelArticulo.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.View;


//Instanciamiento de variables.
public class PanelArticulo extends JPanel
{

    JLabel lImagenArticulo, lNombreArticulo,lNombreArticulo1, lCodigo,lCodigo1, lDescripcion, 
           lPrecioBase, lPrecioBase1;
    JButton bAcceder;
    JPanel izquierda, arriba, abajo, derecha, pDescripcion, panelInformacion;
    JTextPane tDescripcion;
    JScrollPane sDescripcion;
    Container contenedor;
    
    
    //Constructor de la clase.
    public PanelArticulo(Controlador control)//(Controlador control)
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBackground(new Color(206,206,206));
        this.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        lImagenArticulo = new JLabel(iconResize("Imagenes/prueba.jpg", 15, 15));
        lImagenArticulo.setBorder(BorderFactory.createBevelBorder(WIDTH));
        
        lNombreArticulo = new JLabel();
        lCodigo = new JLabel();
        lPrecioBase = new JLabel();
        lNombreArticulo1 = new JLabel("Nombre: ");
        lCodigo1 = new JLabel("Codigo: ");
        lPrecioBase1 = new JLabel("Precio: ");
        
        tDescripcion = new JTextPane();
        tDescripcion.setEditable(false);
        tDescripcion.setBackground(new Color(238,238,238));
        tDescripcion.setContentType("text/html");
        
        
        
        
        pDescripcion = new JPanel();
        
        sDescripcion = new JScrollPane(tDescripcion);
        sDescripcion.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        sDescripcion.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sDescripcion.setPreferredSize(new Dimension(20,40));
        lDescripcion = new JLabel("Desc:");
        
        bAcceder = new JButton("Ingresar a Subasta");
        
        izquierda = new JPanel();
        derecha = new JPanel();
        derecha.setLayout(new BoxLayout(derecha, BoxLayout.Y_AXIS));
        
        arriba = new JPanel();
        abajo = new JPanel();
        
        
        panelInformacion= new JPanel();    
        panelInformacion.setLayout(new GridLayout(4, 2, 0, 0));
        panelInformacion.add(lNombreArticulo1);
        panelInformacion.add(lNombreArticulo);
        panelInformacion.add(lCodigo1);
        panelInformacion.add(lCodigo);
        panelInformacion.add(lPrecioBase1);
        panelInformacion.add(lPrecioBase);
        panelInformacion.add(lDescripcion);
       
        //ubicaciÃ³n
        
        izquierda.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        izquierda.add(lImagenArticulo);
        
        derecha.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        derecha.add(panelInformacion);
        derecha.add(sDescripcion);

        
        abajo.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        abajo.add(bAcceder);
        
        arriba.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        arriba.add(izquierda);
        arriba.add(derecha);
 
        
        this.add(arriba);
        this.add(abajo);
        
        asignarEscuchas(control);
        setVisible(true);
        
        
        
    }
    
    
    
     /**
     * DescripciÃ³n: MÃ©todo encargado de recibir la ubicaciÃ³n de un archivo imagen y dos enteros
     *              que representan el alto y el ancho que tendrÃ¡ la imagen. Esto con el objetivo de 
     *              darle el tamaÃ±o que deseamos a la imagen.
     * ParÃ¡metros de entrada: "url" de tipo String, "width" y "height" de tipo entero.
     * ParÃ¡metros de salida: Una imagen con un tamaÃ±o que hemos definido.
     */
    public ImageIcon iconResize(String url, int width, int height )
    {
          ImageIcon icon; 
          icon = new ImageIcon(getClass().getResource(url));
          Image img = icon.getImage() ;  
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          Image newimg = img.getScaledInstance( screenSize.width/width, screenSize.width/height,  java.awt.Image.SCALE_AREA_AVERAGING) ; 
          icon.setImage(newimg);
          return icon;

    }
    
  /**
 *  Descripción: Método encargado de quitar la escucha al botón "ingresar a subasta".
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Ninguno.
 */    
    public void quitarEscuchas()
    {
        bAcceder.removeActionListener(null);       
    }
    
    
    
/**
 *  Descripción: Método encargado de asignarle la escucha al botón "ingresar a subasta".
 *  Parámetros de entrada: Ninguno.
 *  Parámetros de salida: Ninguno.
 */    
    
    public void asignarEscuchas(Controlador control)
    {
        bAcceder.addActionListener(control);
        
    }

    
 }

