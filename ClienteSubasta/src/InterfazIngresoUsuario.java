
/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: InterfazIngresoUsurio.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias.
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.MenuBar;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.border.Border;
import javax.swing.text.StyleConstants;



//Instanciamiento de las variables.
public class InterfazIngresoUsuario extends JFrame
{
    
    JLabel lIngresoUsuario, lTitulo, lHora, lHoraServidor;
    JTextField tfIngresoUsuario;
    JButton  bIngresar, bSalir;
    Container contenedor;
    JMenuItem miAyuda;
    JPanel panelprincipal, panelHora, panelTitulo, panelCentro, panelSuperior, panelInferior;
    
   
    
    //Constructor de la clase
    public InterfazIngresoUsuario()
    {
        //Contenedor principal.
        super("SubastaPI");
        contenedor = getContentPane();
        
        //Barra menú de selección.
        JMenuBar BarraMenu = new JMenuBar();
        setJMenuBar(BarraMenu);
        
        //Opciones de la barra de menú.
        JMenu opciones = new JMenu("Opciones");
        BarraMenu.add(opciones);
        miAyuda = new JMenuItem("Ayuda");
        opciones.add(miAyuda);
        
        //Labels de información
        lIngresoUsuario = new JLabel("Digite Usuario");
        lTitulo = new JLabel(iconResize("Imagenes/Banner.jpg", 5, 10));
        lHora = new JLabel("- offline -");
        lHoraServidor = new JLabel(" Hora servidor :");
        lHoraServidor.setIcon(iconResize("Imagenes/clock.png", 90, 90));
        tfIngresoUsuario = new JTextField();
        
        //Botones de la ventana
        bIngresar = new JButton("Ingresar");
        bSalir = new JButton("Salir");
        
        //Paneles de ubicación de los elementos de la ventana
        panelprincipal = new JPanel();
        panelHora = new JPanel();
        panelTitulo = new JPanel();
        panelSuperior = new JPanel();
        panelCentro = new JPanel();
        panelInferior = new JPanel();
        panelHora.setLayout(new FlowLayout());
        panelHora.add(lHoraServidor);
        panelHora.add(lHora);
        panelTitulo.add(lTitulo);
        panelSuperior.setBorder(BorderFactory.createEtchedBorder(Color.lightGray, Color.white));
        panelSuperior.setLayout(new BorderLayout());
        panelSuperior.add(panelHora, BorderLayout.WEST);
        panelSuperior.add(panelTitulo, BorderLayout.NORTH);
        panelCentro.setBorder(BorderFactory.createEtchedBorder(Color.lightGray, Color.white));
        panelCentro.setLayout(new GridLayout(1, 2));
        panelCentro.add(lIngresoUsuario);
        panelCentro.add(tfIngresoUsuario);
        panelInferior.setBorder(BorderFactory.createEtchedBorder(Color.lightGray, Color.white));
        panelInferior.add(bIngresar);
        panelInferior.add(bSalir);
        panelprincipal.setLayout(new BorderLayout());
        panelprincipal.add(panelSuperior, BorderLayout.NORTH);
        panelprincipal.add(panelCentro, BorderLayout.CENTER);
        panelprincipal.add(panelInferior, BorderLayout.SOUTH);
        

        contenedor.add(panelprincipal);
        setIconImage(new ImageIcon(getClass().getResource("Imagenes/iconoSubasta.png")).getImage());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        pack();
        
    }   
       
    
    /**
     * Descripción: Método encargado de asignar la escucha de los botones que tiene la ventana,
     *              recibe como parámetro un objeto de la clase controlador, esto hace que
     *              cada vez que se clickée un botón, se genere una acción.
     * Parámetro de entrada: Un objeto de tipo controlador.
     * Parámetro de salida: Ninguno;
     */
    public void asignarEscuchas(Controlador cont)
    {
        bIngresar.addActionListener(cont);
        bSalir.addActionListener(cont);
    }
        
    
    /**
     * Descripción: Método encargado de quitar o deshabilitar la escucha de los botones que tiene la ventana,
     *              recibe como parámetro un objeto de la clase controlador.
     * Parámetro de entrada: Un objeto de tipo controlador.
     * Parámetro de salida: Ninguno;
     */
    public void quitarEscuchas (Controlador cont)
    {
        bIngresar.removeActionListener(cont);
        
    }        
        
    public ImageIcon iconResize(String url, int width, int height )
    {
          ImageIcon icon; 
          icon = new ImageIcon(getClass().getResource(url));
          Image img = icon.getImage() ;  
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          Image newimg = img.getScaledInstance( screenSize.width/width, screenSize.width/height,  java.awt.Image.SCALE_AREA_AVERAGING) ; 
          icon.setImage(newimg);
          return icon;
    }   
        
           
}