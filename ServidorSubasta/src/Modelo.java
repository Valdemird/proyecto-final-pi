
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class Modelo {
    AccesoDatos acceso;
    HiloHora multiCast;
    HiloBuscarConexion uniCast;
    Informador informador;
    private long tiempoDuracionSubasta;
   
    public Modelo() {
        multiCast = new HiloHora();
        uniCast = new HiloBuscarConexion(this);
        acceso = new AccesoDatos();
        multiCast.start();
        uniCast.start();
        informador = new Informador();
        tiempoDuracionSubasta = 60000;
        
    }
    
    public synchronized boolean agregarArticulo(Articulo art){
        boolean exito = true;
        int row = acceso.ingresarArticulos(art.getDescripcion(), art.getUrlImagen(),art.getCosto() , art.getPrecioBase(),art.getPrecioFinal(), art.getCodigoCategoria(), art.getNombre());
        if(row == -1)
            exito = false;
        
       return exito;

    }
    
    public synchronized boolean agregarCliente(String nombre, int saldo){
        boolean exito = true;
        int row = acceso.ingresarCliente(nombre, saldo);
        if(row == -1)
            exito = false;
        
       return exito;
    }
    

        
        public synchronized ArrayList<Articulo> tomarArticulos(String nombreCategoria) 
        {
            ResultSet rs = acceso.consultarArticulos(this.tomarCodigoCategoria(nombreCategoria));
            ArrayList<Articulo> temporal = new ArrayList<Articulo>();
            
        try {
            while(rs.next())
            {
                Articulo actual = new Articulo();
                
                
                actual.setDescripcion((String)rs.getObject(1));
                actual.setUrlImagen((String)rs.getObject(2));
                actual.setCosto((int)rs.getObject(3));
                actual.setPrecioBase((int)rs.getObject(4));
                actual.setPrecioFinal((int)rs.getObject(5));
                actual.setNombre((String)rs.getObject(6));
                actual.setCodigo((int)rs.getObject(7));
                actual.setCodigoCategoria((int)rs.getObject(8));
                temporal.add(actual);
                
            }
        } catch (SQLException ex) {
            System.out.println("Modelo.tomarArticulos()");
        }
        return temporal;
        }
        
        public ArrayList<Articulo> tomarArticulosOrdenadosGanancia(String nombreCategoria)
        {
            ResultSet rs = acceso.consultarArticulosOrdenadoGanancia(this.tomarCodigoCategoria(nombreCategoria));
            ArrayList<Articulo> temporal = new ArrayList<Articulo>();
            
        try {
            while(rs.next())
            {
                Articulo actual = new Articulo();
                
                
                actual.setDescripcion((String)rs.getObject(1));
                actual.setUrlImagen((String)rs.getObject(2));
                actual.setCosto((int)rs.getObject(3));
                actual.setPrecioBase((int)rs.getObject(4));
                actual.setPrecioFinal((int)rs.getObject(5));
                actual.setNombre((String)rs.getObject(6));
                actual.setCodigo((int)rs.getObject(7));
                actual.setCodigoCategoria((int)rs.getObject(8));
                temporal.add(actual);
                
            }
        } catch (SQLException ex) {
            System.out.println("Modelo.tomarArticulos()");
        }
        return temporal;
        }                
        
        public synchronized Articulo tomarArticulo(int codigoArticulo)
        {
            ResultSet rs = acceso.consultarArticulo(codigoArticulo); 
            Articulo articuloPedido = new Articulo();
            
        try {
            while(rs.next())
            {

                
                articuloPedido.setDescripcion((String)rs.getObject(1));
                articuloPedido.setUrlImagen((String)rs.getObject(2));
                articuloPedido.setCosto((int)rs.getObject(3));
                articuloPedido.setPrecioBase((int)rs.getObject(4));
                articuloPedido.setPrecioFinal((int)rs.getObject(5));
                articuloPedido.setNombre((String)rs.getObject(6));
                articuloPedido.setCodigo((int)rs.getObject(7));
                articuloPedido.setCodigoCategoria((int)rs.getObject(8));
                
            }
        } catch (SQLException ex) {
            System.out.println("Modelo.tomarArticulos()");
        }
        return articuloPedido;
        }
        
        
        
        
        public synchronized ArrayList<String> tomarCategorias() 
        {
            ResultSet rs = acceso.consultarCategorias();
            ArrayList<String> temporal = new ArrayList<String>();    
           
        try {
            while (rs.next())
            {                
                temporal.add((String)rs.getObject(1));
            }
        } catch (SQLException ex) {
            System.out.println("Modelo.tomarcategorias");
        }
          return temporal;
        }
        
        public synchronized int tomarCodigoCategoria(String Nombre)
        {
            Integer codigoCategoria = 0;
            ResultSet rs = acceso.consultarCategorias(Nombre); 
            
            try {
            rs.next();
            codigoCategoria = (Integer)rs.getObject(3);
            }catch (SQLException ex) {
            ex.printStackTrace();
            }
            return codigoCategoria;
        }
        
        public synchronized  int consultarSaldoCliente(String nombre)
        {
            int saldo = -1;
            ResultSet rs = acceso.consultarCliente(nombre);
            
            try {
                rs.next();
                saldo = (int)rs.getObject(2);
            } catch (Exception e) {
            } 
        return saldo;
        }
        
    public synchronized boolean CrearSubasta(int codigoArticulo){
        boolean exito = true;
        Date actual = new Date();
        int row = acceso.CrearSubasta(new Time(actual.getTime()), codigoArticulo);
        if(row == -1)
            exito = false;
        
       return exito;
    }
    
    public synchronized boolean CrearOferta(Oferta oferta){
        boolean exito = true;
        Date actual = new Date();
        int row = acceso.CrearOferta(oferta.getValor(), oferta.getCodigoSubasta(), oferta.getNombreCliente(),new Time(actual.getTime()));
        if(row == -1)
            exito = false;
        
       return exito;
    }
    
    public synchronized boolean ingresarSubasta(int codigoArticulo){
        boolean exito = true;
        int row = acceso.AccederSubasta(codigoArticulo);
        if(row == -1)
            exito = false;
       return exito;    
    
    }
    
    public synchronized  Subasta tomarSubasta(int codigoArticulo)
    {
        Subasta temporal = null;
        ResultSet rs = acceso.consultarSubasta(codigoArticulo);
            
        try {
            if(rs.next())
                temporal = new Subasta();
                temporal.setCodigo((int)rs.getObject(1));
                temporal.setHoraInicio((Time)rs.getObject(2));
                temporal.setParticipantes((int)rs.getObject(3));
                temporal.setCodigoArticulo((int)rs.getObject(4));
            } catch (Exception e) {
            } 
        return temporal;
    }
    
    public synchronized  Subasta tomarSubastaPorCodigo(int codigoSubasta)
    {
        Subasta temporal = null;
        ResultSet rs = acceso.consultarSubastaPorCodigo(codigoSubasta);
            
        try {
            if(rs.next())
                temporal = new Subasta();
                temporal.setCodigo((int)rs.getObject(1));
                temporal.setHoraInicio((Time)rs.getObject(2));
                temporal.setParticipantes((int)rs.getObject(3));
                temporal.setCodigoArticulo((int)rs.getObject(4));
            } catch (Exception e) {
            } 
        return temporal;
    }
    
    public synchronized ArrayList<Oferta> tomarOfertas(int codigoSubasta)
        {
            ResultSet rs = acceso.consultarOfertas(codigoSubasta);
            ArrayList<Oferta> temporal = new ArrayList<Oferta>();
            
        try {

            while(rs.next())
            {
                Oferta actual = new Oferta((int)rs.getObject(5),(String)rs.getObject(2),(int)rs.getObject(1));
                actual.setCodigo((int)rs.getObject(3));
                actual.setHoraOferta((Time)rs.getObject(4));
                temporal.add(actual); 
            }
        } catch (SQLException ex) {
            System.out.println("Modelo.tomarArticulos()");
        }
        return temporal;
        }
    
    public ArrayList<Oferta> tomarOfertasMayores(int codigoSubasta)
    {
       ArrayList<Oferta> listaOfertas = tomarOfertas(codigoSubasta);
        System.out.println("listaofertas " + listaOfertas);
       ArrayList<Oferta> listaMayoresOfertas = new ArrayList<Oferta>();
       
        while (listaOfertas.size() != 0) 
        {
            Oferta mayorOferta = listaOfertas.get(0);
            String nombre = mayorOferta.getNombreCliente();
            listaMayoresOfertas.add(mayorOferta);
            ArrayList<Oferta> listaParaEliminar = new ArrayList<Oferta>();
            for(Oferta actual : listaOfertas)
            {
                if(actual.getNombreCliente().equals(nombre))
                {
                    listaParaEliminar.add(actual);
                }
            }
            listaOfertas.removeAll(listaParaEliminar);
        }
        System.out.println("Lista Mayores =" + listaMayoresOfertas);
        return listaMayoresOfertas;
    }
    
        public synchronized  Oferta EstaEnSubasta(String nombrecliente,int codigoSubasta)
    {
        Oferta temporal = null;
        ResultSet rs = acceso.consultarOferta(nombrecliente,codigoSubasta);
            
        try {
            if(rs.next())
                temporal = new Oferta((int)rs.getObject(5),(String)rs.getObject(2), (int)rs.getObject(1));
                temporal.setCodigo((int)rs.getObject(3));
                temporal.setHoraOferta((Time)rs.getObject(4));

            } catch (Exception e) {
            } 
        return temporal;
    }
        
        
        public synchronized boolean SolicitudOferta(Oferta oferta)
        {
            boolean resultado = false;
            ArrayList<Oferta> listaOfertas = tomarOfertas(oferta.getCodigoSubasta());
            Articulo articuloDeInteres = tomarArticulo(tomarSubastaPorCodigo(oferta.getCodigoSubasta()).getCodigoArticulo()); 
            if(oferta.getValor() > listaOfertas.get(0).getValor() && articuloDeInteres.getPrecioFinal() == 0 && oferta.getValor() > articuloDeInteres.getPrecioBase())
            {
                resultado = CrearOferta(oferta);
            }
            return resultado;
        }
        
        public synchronized void IniciarSubasta(int codigoSubasta)
    {
        Date actual = new Date();
        int row = acceso.actualizarHoraSubasta(new Time(actual.getTime()), codigoSubasta);

    }
        
        public synchronized  void actualizarPrecioFinal(int codigoArticulo,int precioFinal)
        {
            int row = acceso.actualizarPrecioFinal(codigoArticulo, precioFinal);
        
        }

    public long getTiempoDuracionSubasta() {
        return tiempoDuracionSubasta;
    }

    public void setTiempoDuracionSubasta(long tiempoDuracionSubasta) {
        this.tiempoDuracionSubasta = tiempoDuracionSubasta*1000;
    }

    
    public ArrayList<Object> categoriaMasVendida()
    {
        int valorCategoriaAnterior = 0;
        String categoria = "";
        ArrayList<String> categorias = tomarCategorias();
        
        for(String actual : categorias)
        {
            int cantidadElementosVendidos = 0;
            ArrayList<Articulo> articulos = tomarArticulos(actual);
            for(Articulo articulo : articulos)
            {
                if(articulo.getPrecioFinal() != 0)
                {
                 cantidadElementosVendidos++;
                }
            }
            if(cantidadElementosVendidos > valorCategoriaAnterior)
            {
                valorCategoriaAnterior = cantidadElementosVendidos;
                categoria = actual;
            }
        }
        ArrayList<Object> respuesta = new ArrayList<>();
        respuesta.add(categoria);
        respuesta.add(valorCategoriaAnterior);
        return respuesta;
    }
}
