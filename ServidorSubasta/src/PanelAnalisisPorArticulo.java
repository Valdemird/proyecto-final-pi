import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import static java.awt.image.ImageObserver.WIDTH;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author GuillermoAndres
 */
public class PanelAnalisisPorArticulo  extends JPanel
{
    
    JLabel lImagen,lCategoria,lCodigoCategoria, lCodigoArticulo, lNumeroCodigoArticulo,
           lPrecioVentaArticulo, lValorPrecioVentaArticulo;
    JButton bRegresar;
    JPanel panelSuperiorIzq, panelSuperiorDer, panelInferior, panelImagen, panelHora, panelCodigoSubasta,
           panelPrecioVentaArticulo, panelCodigoArticulo, panelBotones, panelArriba, panelAbajo;
    DefaultTableModel modeloTabla;
    JTable tabla;
    JScrollPane barras;
    
    
    public PanelAnalisisPorArticulo()
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBackground(new Color(206,206,206));
        this.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        
        lImagen = new JLabel(iconResize("Imagenes/prueba.jpg", 13, 13));
        lImagen.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        lCategoria = new JLabel("Categoria:  ");
        lCodigoCategoria = new JLabel("02");
        lCodigoArticulo = new JLabel("Código articulo: ");
        lNumeroCodigoArticulo = new JLabel("1");
        lPrecioVentaArticulo = new JLabel("Articulo vendido por $ ");
        lValorPrecioVentaArticulo = new JLabel("34565430");
        bRegresar = new JButton("Regresar");
       
        panelImagen = new JPanel();
        panelImagen.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelImagen.add(lImagen);
        
        panelPrecioVentaArticulo = new JPanel();
        panelPrecioVentaArticulo.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelPrecioVentaArticulo.add(lPrecioVentaArticulo);
        panelPrecioVentaArticulo.add(lPrecioVentaArticulo);
        
        panelCodigoSubasta = new JPanel();
        panelCodigoSubasta.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelCodigoSubasta.add(lCategoria);
        panelCodigoSubasta.add(lCodigoCategoria);
       
        panelSuperiorIzq = new JPanel();
        panelSuperiorIzq.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelSuperiorIzq.setLayout(new BoxLayout(panelSuperiorIzq, BoxLayout.Y_AXIS));
        panelSuperiorIzq.add(panelImagen);
        panelSuperiorIzq.add(panelPrecioVentaArticulo);
        
        
        panelCodigoArticulo = new JPanel();
        panelCodigoArticulo.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelCodigoArticulo.add(lCodigoArticulo);
        panelCodigoArticulo.add(lNumeroCodigoArticulo);
        
        panelBotones = new JPanel();
        panelBotones.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelBotones.add(bRegresar);
        
        panelSuperiorDer = new JPanel();
        panelSuperiorDer.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelSuperiorDer.setLayout(new BoxLayout(panelSuperiorDer, BoxLayout.Y_AXIS));
        panelSuperiorDer.add(panelCodigoSubasta);
        panelSuperiorDer.add(panelCodigoArticulo);
        panelSuperiorDer.add(panelBotones);
  
        modeloTabla = new DefaultTableModel();
        int columna = 2;
        Object[] nombCol = new Object[columna];
        nombCol[0] = "Participantes";
        nombCol[1] = "Última oferta";
        modeloTabla.setColumnIdentifiers(nombCol);
        Object [] datosFila = new Object[columna];
   
                datosFila[1] = "$ 300000";
                datosFila[0] = "Guillermo";
                modeloTabla.addRow(datosFila);
        
        tabla = new JTable(modeloTabla);
        barras = new JScrollPane(tabla);
        panelInferior = new JPanel();
        panelInferior.add(barras);
        
        panelArriba = new JPanel();
        panelArriba.setBorder(BorderFactory.createLoweredBevelBorder());
        panelAbajo = new JPanel();
        panelAbajo.setBorder(BorderFactory.createLoweredBevelBorder());
        panelArriba.add(panelSuperiorIzq);
        panelArriba.add(panelSuperiorDer);
        panelAbajo.add(barras);
        
        this.add(panelArriba);
        this.add(panelAbajo);
        
        setSize(500, 300);
        setVisible(true);
        
        
        
     }        
     public ImageIcon iconResize(String url, int width, int height )
    {
          ImageIcon icon; 
          icon = new ImageIcon(getClass().getResource(url));
          Image img = icon.getImage() ;  
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          Image newimg = img.getScaledInstance( screenSize.width/width, screenSize.width/height,  java.awt.Image.SCALE_AREA_AVERAGING) ; 
          icon.setImage(newimg);
          return icon;

    }
     
     
     
     public void actualizarTablaOfertaArticulo(ArrayList<Oferta> listaOfertas)
     {
         modeloTabla.setRowCount(0);
         for(Oferta actual: listaOfertas)
         {
             Object[] datosFilas = new Modelo[getComponentCount()];
             datosFilas[1] = actual.getNombreCliente();
             datosFilas[2] = actual.getValor();
             modeloTabla.addRow(datosFilas);
         }    
     }
     
    public void actualizarLabelInformacion(Articulo articulo)
     {
         Articulo actual = articulo;
         lImagen.setIcon(iconResize(articulo.urlImagen, 13, 13));
         lNumeroCodigoArticulo.setText(Integer.toString(actual.getCodigo()));
         lPrecioVentaArticulo.setText("Articulo Vendido en: " +Integer.toString(actual.getPrecioFinal()) + " $");
         
     }
     

    public void actualizarListaOfertas(ArrayList<Oferta> listaOfertas)
    {
        modeloTabla.setRowCount(0);
        System.out.println("entro");
        for(Oferta actual : listaOfertas)
        {
               Object [] datosFila = new Object[modeloTabla.getColumnCount()];
                datosFila[0] = actual.getNombreCliente();
                datosFila[1] = actual.getValor() + " $";

                modeloTabla.addRow(datosFila);        
        }
    }
}

