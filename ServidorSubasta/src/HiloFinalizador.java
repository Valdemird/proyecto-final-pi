
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class HiloFinalizador extends Thread{
    
    static final String finalizarSubasta = "fin";
    
    Modelo modelo;
    long tiempo;
    int codigoArticulo;
    public HiloFinalizador(Modelo modelo,int codigoArticulo) {
        this.modelo = modelo;
        tiempo = modelo.getTiempoDuracionSubasta();
        this.tiempo = tiempo;
        this.codigoArticulo = codigoArticulo;
    }
    
    @Override
    public void run()
    {
        try {
            sleep(tiempo);
            Subasta subastaActual = modelo.tomarSubasta(codigoArticulo);
            ArrayList<Oferta> listaOfertas = modelo.tomarOfertas(subastaActual.getCodigo());
            if(listaOfertas.size() > 4)
            {
                Oferta ofertaGanadora =  listaOfertas.get(0);
                modelo.actualizarPrecioFinal(codigoArticulo, ofertaGanadora.getValor());
                modelo.informador.InformarCambios(finalizarSubasta, codigoArticulo, ofertaGanadora.getNombreCliente());
            }
            else
            {
                modelo.informador.InformarCambios(finalizarSubasta, codigoArticulo, "ninguno");
                int codigoSubasta = modelo.tomarSubasta(codigoArticulo).getCodigo();
                modelo.acceso.eliminarSubasta(codigoSubasta);
                modelo.acceso.eliminarOfertas(codigoSubasta);
            
            }
            
            


        } catch (InterruptedException ex) {
            Logger.getLogger(HiloFinalizador.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HiloFinalizador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
}
