import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.MenuBar;
import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.StyleConstants;


public class PanelIngresarArticulo extends JFrame
{
    
    Container contenedor;
    JLabel  lNombreArticulo, lCostosArticulo,
           lPrecioBaseArticulo, lImagenTitulo, lDescripcion;
    JTextField  tfNombreArticulo, tfCostoArticulo,
               tfPrecioBaseArticulo;
    JTextArea taDescripcion;
    JButton bCrear, bRegresar;
    JTabbedPane pestanas;
    JPanel pArriba, pAbajo, pCentro, pPrincipal;
    
    public PanelIngresarArticulo()
    {
        super("Crear Articulo");
        contenedor = getContentPane();
        lImagenTitulo = new JLabel("Aquí va el titulo");
        lNombreArticulo = new JLabel("Nombre del articulo:  ");
        lDescripcion = new JLabel("Descripción del articulo:  ");  
        lCostosArticulo = new JLabel("Costo del articulo:  ");
        lPrecioBaseArticulo = new JLabel("Precio base del articulo: ");
        tfCostoArticulo = new JTextField();
        tfNombreArticulo = new JTextField();
        tfPrecioBaseArticulo = new JTextField();
        
        taDescripcion = new JTextArea(1, 1);
        
        bCrear = new JButton("Crear");
        bRegresar = new JButton ("Regresar");
       
        pArriba = new JPanel();
        pCentro = new JPanel();
        pAbajo = new JPanel();
        pPrincipal = new JPanel();
        
        taDescripcion.setBorder((BorderFactory.createRaisedSoftBevelBorder()));
        pArriba.setBorder((BorderFactory.createLoweredSoftBevelBorder()));
        pCentro.setBorder((BorderFactory.createLoweredSoftBevelBorder()));
        pAbajo.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        
        
        pArriba.add(lImagenTitulo);
        pCentro.setLayout(new BoxLayout(pCentro, BoxLayout.Y_AXIS));
        pCentro.setLayout(new GridLayout(0, 2, 2, 2));
        pCentro.add(lNombreArticulo);
        pCentro.add(tfNombreArticulo);
        pCentro.add(lDescripcion);
        pCentro.add(taDescripcion);
        pCentro.add(lCostosArticulo);
        pCentro.add(tfCostoArticulo);
        pCentro.add(lPrecioBaseArticulo);
        pCentro.add(tfPrecioBaseArticulo);
       
        pAbajo.add(bCrear);
        pAbajo.add(bRegresar);
        pPrincipal.setLayout(new BorderLayout());
        pPrincipal.add(pArriba, BorderLayout.NORTH );
        pPrincipal.add(pCentro, BorderLayout.CENTER);
        pPrincipal.add(pAbajo, BorderLayout.SOUTH);
        contenedor.add(pPrincipal);
        
        
        pack();
        setResizable(false);
        setVisible(false);
        
        
        
        
    }
    
    public void asignarEscuchas(Controlador control)
    {   
     
        bCrear.addActionListener(control);
        bRegresar.addActionListener(control);
      
    }        
    public void quitarEscuchas()
    {
        
        bCrear.removeActionListener(null);
        bRegresar.removeActionListener(null);
        
    }        
    
}
