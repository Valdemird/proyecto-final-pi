
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class Informador {
    MulticastSocket conexion;
    DatagramPacket data;
    byte[] b;
    String mensaje;
    
    public Informador()
    {
        try {
            conexion = new MulticastSocket();
        } catch (IOException ex) {
            Logger.getLogger(Informador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    }
    public void InformarCambios(String accion,int codigo,String nombreCliente) throws UnknownHostException, IOException
    {
        System.out.println("!!" + accion + "!! " + codigo +" !! "+ nombreCliente + " !!");
        System.out.println("Informador.InformarCambios()");
        mensaje = accion;
        mensaje += ":" + Integer.toString(codigo);
        mensaje += ":" + nombreCliente +":";
        System.out.println("mensaje generado informador : " + mensaje);
        b = mensaje.getBytes();
        data  = new DatagramPacket(b, b.length,InetAddress.getByName("235.1.1.2"), 4001);
        conexion.send(data);

         
    }
}
