
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class HiloBuscarConexion extends Thread{

    ServerSocket server;
    Modelo modelo;

    public HiloBuscarConexion(Modelo modelo) {
        this.modelo = modelo;
    }
    
    @Override
    public void run()
    {
        try {
            server = new ServerSocket(12345);
            System.out.println("Puerto Abierto");
        } catch (IOException ex) {
            System.out.println("Error creando el server");
        }
        
        while (true) {            
            try {
                CrearConexion();
            } catch (IOException ex) {
                System.out.println("Error accediendo a cliente");
                
            }
        }
    }
    
    public void CrearConexion() throws IOException
    {
        HiloConexion conexion = new HiloConexion(server.accept(),modelo);
        conexion.start();
    }
    
    
}
