/*
 * Clase que permite la conexion con la Base de datos.
 */


//Imorte de librerias para el uso de los métodos.
import java.sql.*;

public class Fachada
{
    
    private final String url, usuario, pwd;
    private Connection conexion;
    
    public Fachada(){
        url = Constantes.url;
        usuario= Constantes.usuario;
        pwd = Constantes.pwd;
    }

    /**
     * Descrpción: Método que se encarga de entablar conexión con la base de datos,
     * Parametros de entrada: Ninguno.
     * Parámetros de salida: Ninguno.
     */
    public Connection conectarABD(){
        
        try{
            Class.forName("org.postgresql.Driver");
            //System.out.println( "Driver Cargado" );
        
        } catch(ExceptionInInitializerError e){
            System.out.println(" the initialization provoked by this method fails"+e);
            //e.printStackTrace();
        }
        catch (ClassNotFoundException ex) {
            System.out.println( "No se pudo cargar el driver, no se encontró la clase."+ex );
            ex.printStackTrace();
        }
              
        try{
            conexion = DriverManager.getConnection(url, usuario, pwd);
            //System.out.println( "Conexion Abierta" );
            return conexion;
        } catch( SQLException e ) {
            System.out.println( "No se pudo abrir."+e );
            //e.printStackTrace();
            return null;
        }
        
    }
    
    /**
     * Descripción: Método encargado de cerrar la conexión con la base de datos.
     * Parámetros de entrada: Un dato de tipo conexión.
     * Parámetros de salida: Ninguno.
     */
    public void cerrarConexion(Connection c){
            try{
                 c.close();
            } catch( SQLException e ) {
                System.out.println( "No se pudo cerrar."+e );
            }
        }//fin metodo cerrarConexion
    
}
