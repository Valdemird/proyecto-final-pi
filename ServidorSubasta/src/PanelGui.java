



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.MenuBar;
import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.StyleConstants;


public class PanelGui extends JFrame
{
    ArrayList<PanelArticulo> articulos;
    PanelAnalisisPorArticulo panelInformativo;
    PanelCrearArticulo crearArticulo;
    JPanel areaArticulos,panelNavegacion,areaSuperior;
    PanelAnalisis panelAnalisis;
    JComboBox<String> cbCategorias,cbTiempoSubasta;
    Container contenedor;
    JTabbedPane pestanas;
    PanelArticulo prueba;
    JScrollPane lArticulos;
    JLabel lTiempoSubasta, lCategoria;
    
    
    public PanelGui()
    {
        super("Servidor Subasta PI");
        setIconImage(new ImageIcon(getClass().getResource("Imagenes/iconoSubasta.png")).getImage());
        articulos = new ArrayList<PanelArticulo>();
        contenedor = getContentPane();
        pestanas = new JTabbedPane();
        areaSuperior = new JPanel(new FlowLayout(FlowLayout.LEFT));
        areaArticulos = new JPanel();
        panelNavegacion = new JPanel();
        panelAnalisis = new PanelAnalisis();
        areaArticulos.setLayout(new GridLayout(0, 3,2,2));
        lArticulos = new JScrollPane(areaArticulos);
        
        lTiempoSubasta = new JLabel("Tiempo Subastas ");
        lCategoria = new JLabel("Categorias ");
        
        cbCategorias = new JComboBox<String>();
        cbTiempoSubasta = new JComboBox<String>();
        cbTiempoSubasta.addItem("1 minuto");
        cbTiempoSubasta.addItem("2 minuto");
        cbTiempoSubasta.addItem("3 minuto");
        cbTiempoSubasta.addItem("4 minuto");
        cbTiempoSubasta.addItem("5 minutos");

        areaSuperior.add(lCategoria);
        areaSuperior.add(cbCategorias);
        areaSuperior.add(lTiempoSubasta);
        areaSuperior.add(cbTiempoSubasta);
        
        
        contenedor.add(pestanas);
        pestanas.add(panelNavegacion,"Navegacion");
        pestanas.add(panelAnalisis,"Analisis");
        panelInformativo = new PanelAnalisisPorArticulo();
        panelNavegacion.setLayout((new BorderLayout()));
        panelNavegacion.add(areaSuperior,BorderLayout.NORTH);
        panelNavegacion.add(lArticulos, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public void actualizarCategorias (ArrayList<String> catModelo)
    {
     if(cbCategorias.getItemCount() != 0)
         cbCategorias.removeAllItems();
     
     for(String actual : catModelo)
     {
         cbCategorias.addItem(actual);
     }
    } 
    
        public void actualizarArticulos(Controlador control, ArrayList<Articulo> artModelo)
    {
        if(!articulos.isEmpty())
        {
            articulos.clear();
            areaArticulos.removeAll();
        }
            
        
        if(crearArticulo == null)
            crearArticulo = new PanelCrearArticulo(control);
        
        for(int i = 0 ; i<artModelo.size(); i++)
        {
            PanelArticulo actual = new PanelArticulo(control);
            actual.lNumCodigo.setText(Integer.toString(artModelo.get(i).getCodigo()) );
            actual.tDescripcion.setText(artModelo.get(i).getDescripcion());
            actual.lImagenArticulo.setIcon(actual.iconResize(artModelo.get(i).getUrlImagen(), 15, 15));
            actual.lValorCosto.setText(Integer.toString(artModelo.get(i).getCosto()) + " $" );
            actual.lValorPrecioBase.setText(Integer.toString(artModelo.get(i).getPrecioBase()) + " $");
            actual.lValorPrecioFinal.setText(Integer.toString(artModelo.get(i).getPrecioFinal()) + " $");
            actual.lNombreArticulo.setText(artModelo.get(i).getNombre());
            if(artModelo.get(i).getPrecioFinal() == 0)
            {
                actual.bVerEstado.setEnabled(false);
            
            }
            else
            {
                actual.bVerEstado.setEnabled(true);
            }
            
            articulos.add(actual);
        }
        contenedor = getContentPane();
        
        for(JPanel actual : articulos)
            areaArticulos.add(actual);
        
        areaArticulos.add(crearArticulo);
        
        if(!this.isVisible())
            setVisible(true);
        pack(); 
    
    }
        
    public void cambiarAnalisis()
    {
        panelNavegacion.remove(lArticulos);
        panelNavegacion.add(panelInformativo, BorderLayout.CENTER);
        cbCategorias.setVisible(false);
        pack();
        
        
    }
    
    public void cambiarAListaArticulos()
    {
        cbCategorias.setVisible(true);
       panelNavegacion.remove(panelInformativo);
       panelNavegacion.add(lArticulos, BorderLayout.CENTER);
       pack();
    }
    
    public int pedirCodigoPanel(JButton boton)
    {
        for(PanelArticulo actual : articulos)
        {
            if(boton.equals(actual.bVerEstado))
            {
                int codigo = Integer.parseInt(actual.lNumCodigo.getText());
                return codigo;
            }
        
        }
        return -1;
    }
    
    public void quitarEscuchas()
    {
        cbCategorias.removeActionListener(null); 
    }
    
    public void asignarEscuchas(Controlador control)
    {
        cbCategorias.addActionListener(control);
        panelInformativo.bRegresar.addActionListener(control);
        cbTiempoSubasta.addActionListener(control);
      
    }        
}
