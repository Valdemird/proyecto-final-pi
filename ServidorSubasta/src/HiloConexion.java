
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class HiloConexion extends Thread{
    
    static final String pedirArticulos = "pedirArticulos";
    static final String pedirCategorias = "pedirCategorias";
    static final String pedirSaldoCliente = "pedirSaldoCliente";
    static final String ingresarCliente = "ingresarCliente";
    static final String ingresarSubasta = "ingresarSubasta";
    static final String generarOferta = "generarOferta";
    static final String actualizarOfertas = "actualizarOfertas";
    static final String actualizarContadorSubasta = "actualizarContadorSubasta";
    static final String solicitarSubasta = "solicitarSubasta";
    static final String solicitarDuracion = "solicitarDuracion";
    
    int codigoSubasta;
    Socket conexionCliente;
    Modelo modelo;
    ObjectInputStream entrada;
    ObjectOutputStream salida;
    public HiloConexion(Socket conexion,Modelo modelo) {
        codigoSubasta = -1;
        this.modelo = modelo;
        conexionCliente = conexion;

        
    }
    
    @Override
    public void run()
    {
        
        try {
            obtenerFlujos();
            while (true) {                
                procesar();                
            }

        } catch (IOException ex) {
            System.out.println("se desconecto un cliente");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error clase no encontrada exepcion");
        }
        finally{
            desconectar();
        }    
    }
    
    private void obtenerFlujos() throws IOException
    {
            salida = new ObjectOutputStream(this.conexionCliente.getOutputStream());
            salida.flush();
            entrada = new ObjectInputStream(this.conexionCliente.getInputStream());

    }
    private void procesar() throws IOException, ClassNotFoundException
    {
        
        ArrayList<Object> mensaje = (ArrayList<Object>)entrada.readObject();
        String instruccion = (String)mensaje.get(0);
        System.out.println("instruccion " + instruccion);
        
        switch(instruccion){
            case pedirArticulos :
                String categoriaEnCliente = (String)mensaje.get(1);
                ArrayList<Articulo> articulosParaEnviar = modelo.tomarArticulos(categoriaEnCliente);
                salida.writeObject(articulosParaEnviar);
                salida.flush();

                break;
            case  pedirCategorias:
                
                ArrayList<String> categoriasParaEnviar = modelo.tomarCategorias();
                salida.writeObject(categoriasParaEnviar);
                salida.flush();
                break;
                
            case pedirSaldoCliente:
                Integer saldoParaEnviar = modelo.consultarSaldoCliente((String)mensaje.get(1));
                salida.writeObject(saldoParaEnviar);
                salida.flush();
                break;
            
            case ingresarSubasta:
                int codigoArticulo = (int)mensaje.get(1);
                String nombreUsuario = (String)mensaje.get(2);
                ArrayList<Object> mensajeSalida =  new ArrayList<Object>();
                
                if(modelo.tomarSubasta(codigoArticulo) == null)
                {
                    modelo.CrearSubasta(codigoArticulo);
                    int codigoSubasta = modelo.tomarSubasta(codigoArticulo).getCodigo();
                    modelo.CrearOferta(new Oferta(codigoSubasta, nombreUsuario, 0));
                }
                else
                {
                    if(modelo.EstaEnSubasta(nombreUsuario,modelo.tomarSubasta(codigoArticulo).getCodigo()) == null)
                   {
                       modelo.ingresarSubasta(codigoArticulo);
                       Subasta subastaActual = modelo.tomarSubasta(codigoArticulo);
                       if(subastaActual.getParticipantes() == 4)
                       {
                           modelo.IniciarSubasta(subastaActual.getCodigo());
                           HiloFinalizador cierreSubasta = new HiloFinalizador(modelo, codigoArticulo);
                           cierreSubasta.start();
                       }
                       int codigoSubasta = subastaActual.getCodigo();
                       modelo.CrearOferta(new Oferta(codigoSubasta, nombreUsuario, 0));
                   }                     
                }
                Subasta SubastaParaEnviar = modelo.tomarSubasta(codigoArticulo);
                Articulo ArticuloParaEnviar = modelo.tomarArticulo(codigoArticulo);
                ArrayList<Oferta> ofertasParaEnviar = modelo.tomarOfertas(SubastaParaEnviar.getCodigo());
                

                mensajeSalida.add(SubastaParaEnviar);
                mensajeSalida.add(ArticuloParaEnviar);
                mensajeSalida.add(ofertasParaEnviar);
                salida.writeObject(mensajeSalida);
                salida.flush();
                
                    modelo.informador.InformarCambios(actualizarContadorSubasta,codigoArticulo,nombreUsuario);
                
                break;
                
            case generarOferta:
                
                Oferta ofertaRealizada = (Oferta)mensaje.get(1);
                System.out.println("oferta realizada es null" + ofertaRealizada == null);
                mensajeSalida =  new ArrayList<Object>();
                boolean OfertaAdmitida = modelo.SolicitudOferta(ofertaRealizada);
                if(OfertaAdmitida)
                {
                    mensajeSalida.add(OfertaAdmitida);
                    mensajeSalida.add(modelo.tomarOfertas(ofertaRealizada.getCodigoSubasta()));
                    
                }
                else
                {
                    mensajeSalida.add(OfertaAdmitida);
                }


                salida.writeObject(mensajeSalida);
                salida.flush();
                if(OfertaAdmitida)
                    modelo.informador.InformarCambios(actualizarOfertas,ofertaRealizada.getCodigoSubasta(),ofertaRealizada.getNombreCliente());
                break;
                
            case actualizarOfertas:
                int codigoSubasta = (int)mensaje.get(1);
                salida.writeObject(modelo.tomarOfertas(codigoSubasta));

                break;
                
            case actualizarContadorSubasta:
                int codigoDelArticulo = (int)mensaje.get(1);
                salida.writeObject(modelo.tomarSubasta(codigoDelArticulo).getParticipantes());
                break;
                
            case solicitarSubasta:
                codigoArticulo = (int)mensaje.get(1);
                salida.writeObject(modelo.tomarSubasta(codigoArticulo));
                
                
                break;
            case solicitarDuracion:
                salida.writeObject(modelo.getTiempoDuracionSubasta());

        }
        
        
        
    }
    
    private void desconectar()
    {
        try {
            salida.close();
            entrada.close();
            conexionCliente.close();
        } catch (IOException ex) {
            System.out.println("Error al desconectar el servidor");
        }

    }   
    
}
