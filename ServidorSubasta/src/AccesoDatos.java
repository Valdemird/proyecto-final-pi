/**
 * Clase : AccesoDatos 
 *         Esta clase encarga de leer y escribir en la base datos los datos que sean necesarios
 *         para uso del juego 
 *          
 */

//Importe de librerias para uso de los metodos.
import java.sql.*;



public class AccesoDatos
{
    
    Fachada fachada;
    ResultSet respuesta;
    Statement instruccion;
    
    
    
    // constantes de base de datos
        static final String bddClientes = "clientes";
        static final String bddArticulos = "articulos";
        static final String bddCategorias = "categorias";
        static final String bddOfertas = "ofertas";
        static final String bddSubastas = "subastas";
        
        static final String artPrecioFinal ="preciofinal";
    
    public AccesoDatos(){
        fachada = new Fachada();
    }
    
    
    /**
     * Descrpción: Método encargado de recibir un dato de tipo String que representa el nombre del
     *             usuario que esté jugando, e ingresarlo en la base de datos si el jugador es nuevo, 
     *             adicionalmente le asignará el úmero de partidas gandas y perdidas con valor cero.
     * Parpametro de entrada: Un dato de tipo String.
     * Parámetro de salida: Un dato de tipo entero(int).
     * @param codigo
     * @param descripcion
     * @param rutaImagen
     * @param costo
     * @param preciobase
     * @param preciofinal
     * @param codigoCategoria
     * @param nombre
     * @return 
     */
    
    
    public int ingresarArticulos(String descripcion, String rutaImagen, int costo
            , int preciobase, int preciofinal, int codigoCategoria, String nombre)
    {
        int numFilas;
        String consulta="INSERT INTO articulos (descripcion, imagen, costo, preciobase, preciofinal,codigocategoria,nombre) VALUES "
                + "('" + descripcion + "','" + rutaImagen + "'," + costo + "," + preciobase + "," 
                + preciofinal + "," + codigoCategoria + ",'" + nombre + "');";
        
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        
        return numFilas;
    }
    
    
        public int ingresarCliente(String nombre, int saldo)
    {
        int numFilas;
        String consulta="INSERT INTO clientes (nombre, saldo) VALUES "
                + "('" + nombre + "'," + saldo + ");";
        
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        
        return numFilas;
    }
    
    public int ingresarCategoria(String codigo, String nombre, String descripcion)
    {
        int numFilas;
        String consulta="INSERT INTO categorias (codigo, nombre, descripcion) VALUES "
                + "('" + codigo + "','" + nombre + "','" + descripcion + "');";
        
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        return numFilas;
    }
    
        public int CrearOferta(int valor, int codigoSubasta, String nombreCliente, Time horaOferta)
    {
        int numFilas;
        String consulta="INSERT INTO ofertas (valor, nombrecliente, horaoferta, codigosubasta) VALUES "
                + "('" + valor + "','" + nombreCliente + "','"+ horaOferta+"','"+ codigoSubasta +"');";
        
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        return numFilas;
    }
        
        
    public int CrearSubasta( Time hora, int codigoArticulo)
    {
        int numFilas;
        String consulta="INSERT INTO subastas (horainicio, codigoarticulo) VALUES "
                + "('" + hora + "','" + codigoArticulo + "');";
        System.out.println("consultar : " + consulta);
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        return numFilas;
    }
    
    public int actualizarHoraSubasta(Time hora, int codigoSubasta)
    {
        int numFilas;
        System.out.println("**************************************");
        System.out.println("hora " + hora.toString());
        System.out.println("**************************************");
        String consulta="UPDATE subastas "
                + "SET horainicio = '"+ hora +"' WHERE codigo = '"+ codigoSubasta +"'";

        System.out.println("consultar : " + consulta);
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        return numFilas;
    }
    
    public int AccederSubasta(int codigoArticulo)
    {
        int numFilas;
        String consulta="UPDATE subastas "
                + "SET participantes = participantes + 1 WHERE codigoarticulo = " + codigoArticulo +" ";

        
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        return numFilas;
    }
    
    
    
    public int eliminarSubasta(int codigoSubasta)
    {
        int numFilas;
        String consulta="DELETE FROM subastas WHERE codigo = '" + codigoSubasta +"';";
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        return numFilas;
    }
    
        public int eliminarOfertas(int codigoSubasta)
    {
        int numFilas;
        String consulta="DELETE FROM ofertas WHERE codigosubasta = " + codigoSubasta +";";
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        return numFilas;
    }
    


    
    
        public int actualizarPrecioFinal(int codigo , int nuevoValor)
    {
        int numFilas;
        String consulta="UPDATE articulos SET preciofinal = "+ nuevoValor +" WHERE codigo ="+ codigo +";";
        try{
           Connection con = fachada.conectarABD();
           instruccion = con.createStatement();
           numFilas = instruccion.executeUpdate(consulta);
           fachada.cerrarConexion(con);
        }catch(SQLException sqle){
            System.out.println("Error de Sql al conectar en programa \n"+sqle);
            numFilas = -1;
        }
        catch(Exception e){
            System.out.println("Ocurrió cualquier otra excepcion en programa"+e);
            numFilas = -1;
        }
        
        return numFilas;
    }
    
    
    /**
     * Descripción: Método encargado de leer en la base de datos la tabla de jugadores y
     *              traerla para su impresión cuando sea requerida.
     * Parámetro de entrada: Ninguno.
     * Parámetro de salida: Tabla de jugadores traida desde la base de datos.
     */
    public ResultSet consultarArticulos (int codigoCategoria)
    {
        String consulta ="SELECT * FROM articulos WHERE codigocategoria = " + codigoCategoria + "ORDER BY preciofinal DESC; ;";

        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        
        return respuesta;
    }
    
    public ResultSet consultarArticulosOrdenadoGanancia(int codigoCategoria)
    {
        String consulta ="SELECT * FROM articulos WHERE codigocategoria = " + codigoCategoria + "ORDER BY preciofinal-preciobase DESC; ;";

        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        
        return respuesta;
    }
     
    
        public ResultSet consultarOfertas (int codigoSubasta)
    {
        String consulta ="SELECT * FROM ofertas WHERE codigosubasta = " + codigoSubasta + " ORDER BY valor DESC;";

        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        
        return respuesta;
    }
        
    public ResultSet consultarOferta(String nombrecliente, int codigoSubasta)
    {
        String consulta ="SELECT * FROM ofertas WHERE nombrecliente = '" + nombrecliente + "' AND codigosubasta = " + codigoSubasta + " ;";

        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        
        return respuesta;
    }
    
    public ResultSet consultarArticulo(int codigoArticulo)
    {
        String consulta ="SELECT * FROM articulos WHERE codigo = " + codigoArticulo + ";";

        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        
        return respuesta;
    }            
    
    public ResultSet consultarCategorias()
    {
        String consulta ="SELECT * FROM categorias;";

        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        finally{
        return respuesta;
        }
    }
    
    public ResultSet consultarCategorias(String nombre)
    {
        String consulta ="SELECT * FROM categorias WHERE nombre = '" + nombre + "';";
        
        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        finally{
        return respuesta;
        }    
    }
    
    public ResultSet consultarCliente(String nombre)
    {
        String consulta ="SELECT * FROM clientes WHERE nombre = '" + nombre + "';";
        
        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        finally{
        return respuesta;
        }
    }
    
    public ResultSet consultarSubasta(int codigoArticulo)
     {
        String consulta ="SELECT * FROM subastas WHERE codigoarticulo = '" + codigoArticulo + "';";
        
        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        finally{
        return respuesta;
        }
    }
    
    public ResultSet consultarSubastaPorCodigo(int codigoSubasta)
     {
        String consulta ="SELECT * FROM subastas WHERE codigo = '" + codigoSubasta + "';";
        
        try{
            Connection con = fachada.conectarABD();
            instruccion = con.createStatement();
            respuesta = instruccion.executeQuery(consulta);
            fachada.cerrarConexion(con);
               
        }catch(SQLException sqle){
            System.out.println("Error al consultar datos");
            sqle.printStackTrace();
        }
        finally{
        return respuesta;
        }
    }
    
}
