

/*
    Asignatura: PROGRAMACIÓN INTERACTIVA (PI) 750085M
    Archivo: Articulo.java
    Fecha creación: 01-Junio-2016
    Fecha última modificación: 04-Julio-2016
    Versión: 0.1
    Autor: Guillermo Andrés Hernandez Zamora 1527085-2711
           Sebastian Castaño Jara            1332055-2711
    ESCUELA DE INGENIERÍA DE SISTEMAS Y COMPUTACIÓN
    UniValle-2016
*/


//Importe de librerias
import java.io.Serializable;


//Instanciamoiento de las variables.
public class Articulo implements Serializable{
    static final long serialVersionUID = 10L;
    int codigo;
    String descripcion;
    String urlImagen;
    int costo;
    int precioBase;
    int precioFinal;
    int codigoCategoria;
    String Nombre;

    
    
 /**
     * Decripción: Método encargado de retornar un valor de tipo entero (int) que representa el precio final
     *             que toma un articulo, una vez termina la subasta del mismo.
     * Parámetro de entrada: Ninguno;
     * Parámetro de salida: Valor de tipo entero(int).
     */    
    public int getPrecioFinal() 
    {
        return precioFinal;
    }

    
 /**
     * Decripción: Método que recibe un valor de tipo entero (int) que representa el precio final
     *             que toma un articulo y lo asigna a la variable que representa este valor.
     * Parámetro de entrada: Valor de tipo entero (int)
     * Parámetro de salida: Ninguno
     */    
    public void setPrecioFinal(int precioFinal) 
    {
        this.precioFinal = precioFinal;
    }


    
/**
     * Decripción: Método encargado de retornar un valor de tipo String que representa el código asignado
     *             a un articulo.
     * Parámetro de entrada: Ninguno;
     * Parámetro de salida: Valor de tipo String.
     */    
    public int getCodigo() 
    {
        return codigo;
    }



/**
     * Decripción: Método que recibe un valor de tipo String que representa el codigó asignado
     *             a un articulo y lo asigna a la variable que representa este valor.
     * Parámetro de entrada: Valor de tipo String.
     * Parámetro de salida: Ninguno
     */
    public void setCodigo(int codigo) 
    {
        this.codigo = codigo;
    }

    
    
    
/**
     * Decripción: Método encargado de retornar un valor de tipo String que representa la descripción  
     *             que tiene un articulo.
     * Parámetro de entrada: Ninguno;
     * Parámetro de salida: Valor de tipo String.
     */    
    public String getDescripcion() 
    {
        return descripcion;
    }

    
    
/**
     * Decripción: Método que recibe un valor de tipo String que representa la descripción
     *             que tiene un articulo y lo asigna a la variable que representa este valor.
     * Parámetro de entrada: Valor de tipo String.
     * Parámetro de salida: Ninguno
     */    
    public void setDescripcion(String descripcion) 
    {
        this.descripcion = descripcion;
    }

    
    
/**
     * Decripción: Método encargado de retornar un valor de tipo String que representa la dirección de
     *             ubicación de memoria y el nombre de la imagen que representa el articulo.
     * Parámetro de entrada: Ninguno;
     * Parámetro de salida: Valor de tipo String.
     */    
    public String getUrlImagen() 
    {
        return urlImagen;
    }

    
    
    
/**
     * Decripción: Método que recibe un valor de tipo String que representala dirección de
     *             ubicación de memoria y el nombre de la imagen querepresenta un articulo y lo asigna 
     *             a la variable que representa este valor.
     * Parámetro de entrada: Valor de tipo String.
     * Parámetro de salida: Ninguno
     */    
    public void setUrlImagen(String urlImagen) 
    {
        this.urlImagen = urlImagen;
    }



 /**
     * Decripción: Método encargado de retornar un valor de tipo entero (int) que representa el precio costo
     *             que tiene un articulo.
     * Parámetro de entrada: Ninguno;
     * Parámetro de salida: Valor de tipo entero(int).
     */
    public int getCosto() 
    {
        return costo;
    }

    
    
/**
     * Decripción: Método que recibe un valor de tipo entero (int) que representa el precio costo
     *             que tiene un articulo y lo asigna a la variable que representa este valor.
     * Parámetro de entrada: Valor de tipo entero (int)
     * Parámetro de salida: Ninguno
     */    
    public void setCosto(int costo) 
    {
        this.costo = costo;
    }

    
    
/**
     * Decripción: Método encargado de retornar un valor de tipo entero (int) que representa el precio base
     *             que tendrá un articulo al iniciar la subasta.
     * Parámetro de entrada: Ninguno;
     * Parámetro de salida: Valor de tipo entero(int).
     */    
    public int getPrecioBase() 
    {
        return precioBase;
    }

 
/**
     * Decripción: Método que recibe un valor de tipo entero (int) que representa el precio base
     *             que tomará un articulo al iniciar la subasta y lo asigna a la variable que representa este valor.
     * Parámetro de entrada: Valor de tipo entero (int)
     * Parámetro de salida: Ninguno
     */    
    public void setPrecioBase(int precioBase) 
    {
        this.precioBase = precioBase;
    }

    
    
    
 /**
     * Decripción: Método encargado de retornar un valor de tipo String que representa el código de la categoría
     *             que contiene el un articulo.
     * Parámetro de entrada: Ninguno;
     * Parámetro de salida: Valor de tipo String.
     */    
    public int getCodigoCategoria() 
    {
        return codigoCategoria;
    }

    
    
    
 /**
     * Decripción: Método que recibe un valor de tipo String que representa el codigó de la categoría
     *             que contiene el articulo y lo asigna a la variable que representa este valor.
     * Parámetro de entrada: Valor de tipo String.
     * Parámetro de salida: Ninguno
     */    
    public void setCodigoCategoria(int codigoCategoria) 
    {
        this.codigoCategoria = codigoCategoria;
    }

    
    
    
/**
     * Decripción: Método encargado de retornar un valor de tipo String que representa el nombre del un articulo.
     * Parámetro de entrada: Ninguno;
     * Parámetro de salida: Valor de tipo String.
     */    
    public String getNombre()
    {
        return Nombre;
    }

    
    
/**
     * Decripción: Método que recibe un valor de tipo String que representa el nombre del articulo y 
     *             lo asigna a la variable que representa este valor.
     * Parámetro de entrada: Valor de tipo String.
     * Parámetro de salida: Ninguno
     */    
    public void setNombre(String Nombre)
    {
        this.Nombre = Nombre;
    }
    
}
