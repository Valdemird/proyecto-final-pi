
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class Oferta implements Serializable{
    static final long serialVersionUID = 8L;
    private int valor;
    private int codigoSubasta;
    private String nombreCliente;
    private int codigo;
    private Time horaOferta;

    public Oferta(int codigoSubasta, String nombreCliente, int valor) {
        this.valor = valor;
        this.codigoSubasta = codigoSubasta;
        this.nombreCliente = nombreCliente;
    }
    
    

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getCodigoSubasta() {
        return codigoSubasta;
    }

    public void setCodigoSubasta(int codigoSubasta) {
        this.codigoSubasta = codigoSubasta;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Time getHoraOferta() {
        return horaOferta;
    }

    public void setHoraOferta(Time horaOferta) {
        this.horaOferta = horaOferta;
    }
    
    




}
