
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.text.View;

public class PanelArticulo extends JPanel
{

    JLabel lImagenArticulo, lNombre, lNombreArticulo, lCodigo,lNumCodigo, lCosto, lValorCosto, lDescripcion,
            lPrecioBase, lValorPrecioBase, lPrecioFinal, lValorPrecioFinal;
    JButton bVerEstado;
    JTextPane tDescripcion;
    JPanel izquierda, arriba, abajo, derecha, pDescripcion, panelInformacion;
    JScrollPane sDescripcion;
    
    
    
    public PanelArticulo(Controlador control)
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBackground(new Color(206,206,206));
        this.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        
        lImagenArticulo = new JLabel(iconResize("Imagenes/prueba.jpg", 15, 15));
        lImagenArticulo.setBorder(BorderFactory.createBevelBorder(WIDTH));
        
        lNombre = new JLabel("Nombre: ");
        lNombreArticulo = new JLabel();
        lCodigo = new JLabel("Código: ");
        lNumCodigo = new JLabel();
        lCosto = new JLabel("Costo: ");
        lValorCosto = new JLabel();
        lPrecioBase = new JLabel("Precio base: ");
        lValorPrecioBase = new JLabel();
        lPrecioFinal = new JLabel("Precio Final: ");
        lValorPrecioFinal = new JLabel();
        lDescripcion = new JLabel("Descripción:");
        pDescripcion = new JPanel();
        sDescripcion = new JScrollPane(pDescripcion);
        sDescripcion.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        sDescripcion.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sDescripcion.setPreferredSize(new Dimension(20,40));
        
        
        tDescripcion = new JTextPane();
        tDescripcion.setEditable(false);
        tDescripcion.setBackground(new Color(238,238,238));
        tDescripcion.setContentType("text/html");
        
        pDescripcion = new JPanel();
        
        sDescripcion = new JScrollPane(tDescripcion);
        sDescripcion.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        sDescripcion.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sDescripcion.setPreferredSize(new Dimension(20,40));
        lDescripcion = new JLabel("Descripciòn:");
        
        bVerEstado = new JButton("Estado");       
        
        panelInformacion= new JPanel();    
        panelInformacion.setLayout(new GridLayout(6, 2, 0, 0));
        panelInformacion.add(lNombre);
        panelInformacion.add(lNombreArticulo);
        panelInformacion.add(lCodigo);
        panelInformacion.add(lNumCodigo);
        panelInformacion.add(lCosto);
        panelInformacion.add(lValorCosto);
        panelInformacion.add(lPrecioBase);
        panelInformacion.add(lValorPrecioBase);
        panelInformacion.add(lPrecioFinal);
        panelInformacion.add(lValorPrecioFinal);
        panelInformacion.add(lDescripcion);
        
        
        izquierda = new JPanel();
        derecha = new JPanel();
        derecha.setLayout(new BoxLayout(derecha, BoxLayout.Y_AXIS));
        
        arriba = new JPanel();
        abajo = new JPanel();
       
        //ubicación
        
        izquierda.setBorder(BorderFactory.createRaisedSoftBevelBorder());
        izquierda.add(lImagenArticulo);
        //izquierda.add(bSeleccionar);
        
        
        derecha.setBorder(BorderFactory.createEtchedBorder());
        derecha.add(panelInformacion);
        derecha.add(sDescripcion);
        
        
        abajo.setBorder(BorderFactory.createRaisedSoftBevelBorder());
        abajo.add(bVerEstado);
        
        arriba.setBorder(BorderFactory.createRaisedSoftBevelBorder());
        arriba.add(izquierda);
        arriba.add(derecha);
 
        
        this.add(arriba);
        this.add(abajo);
        
        asignarEscuchas(control);
        setVisible(true);
        
        
        
    }
    
    
    
     /**
     * Descripción: Método encargado de recibir la ubicación de un archivo imagen y dos enteros
     *              que representan el alto y el ancho que tendrá la imagen. Esto con el objetivo de 
     *              darle el tamaño que deseamos a la imagen.
     * Parámetros de entrada: "url" de tipo String, "width" y "height" de tipo entero.
     * Parámetros de salida: Una imagen con un tamaño que hemos definido.
     */
    public ImageIcon iconResize(String url, int width, int height )
    {
          ImageIcon icon; 
          icon = new ImageIcon(getClass().getResource(url));
          Image img = icon.getImage() ;  
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          Image newimg = img.getScaledInstance( screenSize.width/width, screenSize.width/height,  java.awt.Image.SCALE_AREA_AVERAGING) ; 
          icon.setImage(newimg);
          return icon;

    }
    
    public void quitarEscuchas()
    {
        bVerEstado.removeActionListener(null);       
    }
    
    public void asignarEscuchas(Controlador control)
    {
        bVerEstado.addActionListener(control);
    }
    
    public int pedirCodigoArticulo()
    {
        int codigoArticulo = Integer.parseInt(lCodigo.getText());
        return codigoArticulo;
     
    }

    
 }
    

