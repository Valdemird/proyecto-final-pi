

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


public class PanelAnalisis extends JPanel
{
    

    JPanel panelBarras, panelInferior, panelComboBox, panelInformacionArticuloMasOfertado,
           panelInformacionCategoriaMasVendio;
    JLabel lInformacionCategoriaMasVendio, lImagen ;
    JTextArea textAreaNumeroParticipantes, textAreaArticulosMasVendido;
    JScrollPane scrollAnalisis;
    JButton bAnalizar;
    JComboBox cbMenuAnalisis, cbCategorias;
    ImageIcon imagen;

    JFreeChart chartVendidoMenorMayorPrecio,chartArticuloMasGananciaObtuvo;
    ChartPanel panelchartVendidoMayorMenorPrecio, panelchartMayorGanancia;
    DefaultCategoryDataset datasetVendidoMayorMenorPrecio, datasetMayorGanancia;
    
    public PanelAnalisis()
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBackground(new Color(206,206,206));
        this.setBorder(BorderFactory.createLoweredSoftBevelBorder());
       
        
        datasetVendidoMayorMenorPrecio = new DefaultCategoryDataset();
        datasetMayorGanancia = new DefaultCategoryDataset();

        
        imagen = new ImageIcon(getClass().getResource("Imagenes/prueba.jpg"));
        
        lImagen = new JLabel(imagen);
        //Pendiente por quitar
        lInformacionCategoriaMasVendio = new JLabel("La categoria que mas articulos vendio es:       con      Articulos vendidos");
        
        bAnalizar = new JButton("Analizar");
        
        
        cbMenuAnalisis = new JComboBox<String>();
        cbMenuAnalisis.addItem("Articulos vendidos de Mayor a menor precio");
        cbMenuAnalisis.addItem("Articulo que dejó mayor ganancia");
        
        
        
        cbCategorias = new JComboBox<String>();
        cbCategorias.addItem("Carros");
        
        panelBarras = new JPanel();
 
        
        panelComboBox = new JPanel();
        panelComboBox.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelComboBox.add(cbMenuAnalisis);
        panelComboBox.add(cbCategorias);
        panelComboBox.add(bAnalizar);
        
        panelInformacionArticuloMasOfertado = new JPanel();
        
        panelInformacionCategoriaMasVendio = new JPanel();
        panelInformacionCategoriaMasVendio.add(lInformacionCategoriaMasVendio);
        
        
        panelInferior = new JPanel();
        panelInferior.setLayout(new BoxLayout(panelInferior, BoxLayout.Y_AXIS));
        panelInferior.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelInferior.add(panelInformacionCategoriaMasVendio);
        panelInferior.add(panelInformacionArticuloMasOfertado);
        

       
       
  
       
   
         /**
         *     Título del gráfico, "Visitas" en nuestro ejemplo.
               Etiqueta para el eje horizontal, "día" en nuestro caso, que vamos a pintar las barras 
               en vertical y cada barra es un día de la semana.
         *     Etiqueta para el eje vertical, "número de visitas" en nuestro caso, en el que la longitud
               de la barra es el número de visitas.
         *     El modelo de datos dataset.
         *     Si queremos las barras en vertical, PlotOrientation.VERTICAL en nuestro ejemplo, 
               o si las queremos en horizontal.
         *     Un boolean que indica si queremos una etiqueta con el nombre de los sitos web en la parte 
               inferior del gráfico, de forma que podamos ver cada color de las barras a qué sitio web corresponde.
               En el ejemplo hemos puesto true.
         *     Un boolean que indica si queremos tooltip en las barras, de forma que al poner el ratón sobre ellas, 
               aparezca una pequeña etiqueta indicando el valor. Hemos puesto true en el ejemplo.
         *     Un boolean si queremos que se generen urls. Este flag se utiliza si vamos a generar el gráfico para una página web y queremos que en el gráfico haya urls. En nuestro ejemplo hemos puesto false. 
         */

        
        chartVendidoMenorMayorPrecio = ChartFactory.createBarChart3D("Articulos Vendidos de Mayor a Menor Precio", "Articulos",
        "Precio $", datasetVendidoMayorMenorPrecio, PlotOrientation.VERTICAL, true,true, false);
        
        chartArticuloMasGananciaObtuvo = ChartFactory.createBarChart3D("Articulos Con Mayor Ganancia", "Articulos",
                "Utilidad", datasetMayorGanancia, PlotOrientation.VERTICAL, true, true, false);
        
        panelchartVendidoMayorMenorPrecio = new ChartPanel(chartVendidoMenorMayorPrecio);
        panelchartMayorGanancia = new ChartPanel(chartArticuloMasGananciaObtuvo);
        panelBarras.setBorder(BorderFactory.createBevelBorder(WIDTH, new Color(169, 169, 169),new Color(169, 169, 169)));
        panelBarras.add(panelchartMayorGanancia);
        panelBarras.add(panelchartVendidoMayorMenorPrecio);
        this.add(panelBarras);
        this.add(panelComboBox);
        this.add(panelInferior);
        
        setVisible(true);
        
        
    }        

    //Para el combobox de este panel de analisis
    public void actualizarCategorias (ArrayList<String> catModelo)
    {
     if(cbCategorias.getItemCount() != 0)
         cbCategorias.removeAllItems();
     
     for(String actual : catModelo)
     {
         cbCategorias.addItem(actual);
     }
    }
    
    public void asignarEscuchas(Controlador control)
    {
        bAnalizar.addActionListener(control);
    
    }

    //Se debe seleccionar la categoria.
    public void insertarDatosVendidosMayorMenorPrecio(ArrayList<Articulo> artModel)
    {
        datasetVendidoMayorMenorPrecio.clear();
        for(Articulo actual : artModel)
        {    
            if(actual.getPrecioFinal()!= 0)
            {
                int num = actual.getPrecioFinal();
                String articulo = actual.getNombre();
                String categoria = Integer.toString(actual.getCodigoCategoria());
                datasetVendidoMayorMenorPrecio.addValue(num, articulo, categoria);
            }
        }
        chartVendidoMenorMayorPrecio = ChartFactory.createBarChart3D("Articulos Vendidos de mayor a menor Precio", "Articulos",
        "Precio $", datasetVendidoMayorMenorPrecio, PlotOrientation.VERTICAL, true,true, false);

        
        System.out.println("PanelAnalisis.insertarDatosVendidosMayorMenorPrecio()");

    }
    
    public void insertarDatosMayorGanancia(ArrayList<Articulo> artModel)
     {
        datasetMayorGanancia.clear();
        for(Articulo actual : artModel)
        {    
            if(actual.getPrecioFinal()!= 0)
            {
                int Pfinal = actual.getPrecioFinal();
                int Pcosto = actual.getCosto();
                String articulo = actual.getNombre();
                String categoria = Integer.toString(actual.getCodigoCategoria());
                datasetMayorGanancia.addValue(Pfinal-Pcosto, articulo, categoria);
            }
        }
        chartArticuloMasGananciaObtuvo = ChartFactory.createBarChart3D("Articulos Con mayor Ganancia", "Articulos",
        "Utilidad $", datasetMayorGanancia, PlotOrientation.VERTICAL, true,true, false);


    }
    
    public void actualizarArticuloMasVendido(ArrayList<Object> mensaje)
    {
        String categoria = (String)mensaje.get(0);
        int cantidad = (int)mensaje.get(1);
        lInformacionCategoriaMasVendio.setText("La categoria que mas articulos vendio es la categoria : " + categoria + " con " + cantidad + " Articulos vendidos");
    
    }
    
    
       
    
    
}
