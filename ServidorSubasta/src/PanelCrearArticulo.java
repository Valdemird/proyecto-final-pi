

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.text.View;


public class PanelCrearArticulo extends JPanel
{
    
    JButton bCrear;
    JPanel pIzquierda, pDerecha, arriba;
    JLabel imagenPanelCrearArticulo;
    PanelIngresarArticulo ventanaCrearArticulo;
    
    public PanelCrearArticulo(Controlador control)
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBackground(new Color(206,206,206));
        this.setBorder(BorderFactory.createLoweredSoftBevelBorder());
        ventanaCrearArticulo = new PanelIngresarArticulo();
        imagenPanelCrearArticulo = new JLabel(iconResize("Imagenes/interrogacion.png", 16, 16));
        imagenPanelCrearArticulo.setBorder(BorderFactory.createBevelBorder(WIDTH));
        bCrear = new JButton("Ingresar Articulo");
        
        pIzquierda = new JPanel();
        pDerecha = new JPanel();
        arriba = new JPanel();
        
        pIzquierda.add(imagenPanelCrearArticulo);
        pDerecha.add(bCrear);
        arriba.add(pIzquierda);
        arriba.add(pDerecha);
        
        
        this.add(arriba);
        asignarEscuchas(control);
        setVisible(true);
    }        
    
    
    
    
    
    
    private ImageIcon iconResize(String url, int width, int height )
    {
          ImageIcon icon; 
          icon = new ImageIcon(getClass().getResource(url));
          Image img = icon.getImage() ;  
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          Image newimg = img.getScaledInstance( screenSize.width/width, screenSize.width/height,  java.awt.Image.SCALE_AREA_AVERAGING) ; 
          icon.setImage(newimg);
          return icon;

    }
    
    public void desplegarVentana(Controlador control)
    {
        if(ventanaCrearArticulo != null)
        {
            ventanaCrearArticulo.quitarEscuchas();
            ventanaCrearArticulo.setVisible(false);
            ventanaCrearArticulo.dispose();
        }

        ventanaCrearArticulo = new PanelIngresarArticulo();
        ventanaCrearArticulo.asignarEscuchas(control);
        ventanaCrearArticulo.setVisible(true);
    }
    
    public void quitarVentana()
    {
        ventanaCrearArticulo.setVisible(false);
    
    }

    public void quitarEscuchas()
    {
        bCrear.removeActionListener(null);
    }
    
    public void asignarEscuchas(Controlador control)
    {
        bCrear.addActionListener(control);
    }    
    
}
