
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class Controlador implements ActionListener{

    PanelGui interfaz;

    Modelo model;
    public Controlador(PanelGui interfaz) {
    
        this.interfaz = interfaz;
        model = new Modelo();
        interfaz.actualizarCategorias(model.tomarCategorias());
        interfaz.panelAnalisis.actualizarCategorias(model.tomarCategorias());
        interfaz.panelAnalisis.asignarEscuchas(this);
        interfaz.actualizarArticulos(this, model.tomarArticulos(interfaz.cbCategorias.getSelectedItem().toString()));
        interfaz.asignarEscuchas(this);
        

        System.out.println(model.tomarArticulos(interfaz.cbCategorias.getSelectedItem().toString()).size());
    }
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {

       if(e.getSource().equals(interfaz.crearArticulo.bCrear))
       {
           interfaz.crearArticulo.desplegarVentana(this);
       }      
       if(e.getSource().equals(interfaz.cbCategorias))
       {
           interfaz.actualizarArticulos(this, model.tomarArticulos(interfaz.cbCategorias.getSelectedItem().toString()));
       }
       
       if(e.getSource().equals(interfaz.cbTiempoSubasta))
       {
           int posicionActual = interfaz.cbTiempoSubasta.getSelectedIndex() + 1;
           model.setTiempoDuracionSubasta(posicionActual*1000);
           System.out.println(posicionActual);
       }
       
        if(e.getSource().equals(interfaz.crearArticulo.ventanaCrearArticulo.bCrear))
        {
            agregarArticulo(interfaz.crearArticulo.ventanaCrearArticulo);
            interfaz.crearArticulo.quitarVentana();
            interfaz.actualizarArticulos(this, model.tomarArticulos(interfaz.cbCategorias.getSelectedItem().toString()));
            System.out.println("crear");

        }
        if(e.getSource().equals(interfaz.crearArticulo.ventanaCrearArticulo.bRegresar))
        {
            interfaz.crearArticulo.quitarVentana();
        }
        
        if(e.getSource().equals(interfaz.panelInformativo.bRegresar))
        {
            interfaz.cambiarAListaArticulos();
        
        }
        
        if(e.getSource() instanceof JButton)
        {
            JButton boton = (JButton)e.getSource();
            if(boton.getText().equals("Estado"))
            {
                int codigoArticulo = interfaz.pedirCodigoPanel(boton);
                Articulo actual = model.tomarArticulo(codigoArticulo);
                interfaz.panelInformativo.actualizarLabelInformacion(actual);
                interfaz.panelInformativo.actualizarListaOfertas(model.tomarOfertasMayores(model.tomarSubasta(codigoArticulo).getCodigo()));
                interfaz.cambiarAnalisis();
            }

        }
        
        if(e.getSource().equals(interfaz.panelAnalisis.bAnalizar))
        {
            if(interfaz.panelAnalisis.cbMenuAnalisis.getSelectedIndex() == 0)
            {
                ArrayList<Articulo> articulosOrdenado = model.tomarArticulos(interfaz.panelAnalisis.cbCategorias.getSelectedItem().toString());
                interfaz.panelAnalisis.insertarDatosVendidosMayorMenorPrecio(articulosOrdenado);
                interfaz.panelAnalisis.actualizarArticuloMasVendido(model.categoriaMasVendida());           
            }
            else
            {
               ArrayList<Articulo> articulosOrdenado = model.tomarArticulosOrdenadosGanancia(interfaz.panelAnalisis.cbCategorias.getSelectedItem().toString());
               interfaz.panelAnalisis.insertarDatosMayorGanancia(articulosOrdenado);
               interfaz.panelAnalisis.actualizarArticuloMasVendido(model.categoriaMasVendida()); 
            
            }

        
        
        }
    }
    
    public void agregarArticulo(PanelIngresarArticulo ventana)
    {
            Articulo temporal = new Articulo();
            temporal.setCodigoCategoria(model.tomarCodigoCategoria(interfaz.cbCategorias.getSelectedItem().toString()));
            temporal.setCosto(Integer.parseInt(ventana.tfCostoArticulo.getText()));
            temporal.setDescripcion(ventana.taDescripcion.getText());
            temporal.setNombre(ventana.tfNombreArticulo.getText());
            temporal.setPrecioBase(Integer.parseInt(ventana.tfPrecioBaseArticulo.getText()));
            temporal.setPrecioFinal(0);
            int randomNum = 1 + (int)(Math.random() * 15); 
            temporal.setUrlImagen("Imagenes/"+ interfaz.cbCategorias.getSelectedItem().toString() + randomNum +".jpg");

            if( !model.agregarArticulo(temporal))
            {
                JOptionPane.showMessageDialog(ventana, "ya existe el articulo");
            
            }
           


        
    }
  
}
